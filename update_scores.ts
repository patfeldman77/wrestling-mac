var _ = require('lodash');
var fs = require('fs');
// var sc = require('./../../app/util/score.convert.js');
// var iw = require('./../../app/store/models/wrestlers.interface.js')
var weights = ['125', '133', '141','149','157','165','174','184','197','285']; 


enum EDecisionType {
	BYE,
	DEFAULT,
	FORFEIT,
	DISQUALIFICATION,
	FALL,
	TECH_FALL_WITH_NEAR_FALL,
	TECH_FALL,
	DECISION,
	MAJOR,

}

enum EPigTailType {
	CHAMP, 
	CONSOS
}

interface IScore {
	matchDesc: string;
	winner: string;
	loser: string;
	decision: string;
	decisionType: EDecisionType;
	result: string;
	teamPoints: number;
	advancementPoints: number;
	bonusPoints: number;
	placementPoints: number;

}

interface IWrestler{
	id: number;
    rank: number; // 15,
	percentage: number;
	weight: string;
    first_name: string; // "Nicholas",
    last_name: string; // "Gravina",
    wins: number; // 21,
    losses: number; // 9,
	school: string;
    year: string; // "Sophomore",
	rpi: string;
    conf : string;
	matches: IScore[];
}


class ScoreConvert {
	constructor() { }

	static Convert(wrestlersAtWeight: IWrestler[], weight: string, input: string) {
		var lines = input.split('\n');

		wrestlersAtWeight.forEach(wrestler => {			
			var thisWrestlerStats: IScore[] = [];
			var lastCat = EPigTailType.CONSOS;
			lines.forEach(line => {
				if (line.indexOf('Conso Pig Tails')){
					lastCat = EPigTailType.CONSOS;
				} else if (line.indexOf ('Pig Tails')){
					lastCat = EPigTailType.CHAMP;
				}
				if (line.indexOf(wrestler.last_name) >= 0) {
					var details: IScore = this.ParseLine(line, wrestler, lastCat);
					if (!!details) {
						if (details.loser.toLowerCase().indexOf(wrestler.last_name.toLowerCase()) >= 0){
							if (this.CheckFirstName(details.loser, wrestler)){
								thisWrestlerStats.push(details);							
							}
						} else if (details.winner.toLowerCase().indexOf(wrestler.last_name.toLowerCase()) >= 0){
							if (this.CheckFirstName(details.winner, wrestler)){
								thisWrestlerStats.push(details);							
							}
						} else {
							console.log("NAME WARNING:::NOT IN WINNER OR LOSER " + details.loser + " OR " + details.winner + "/" + wrestler.first_name + " " + wrestler.last_name + ":: MIGHT BE A FIRST NAME/LAST NAME CONFUSION");
						}
					}
				}
			});
			wrestler.matches = thisWrestlerStats;
		})
	}

	static CheckFirstName(name: string, wrestler: IWrestler): boolean{
		if (name.toLowerCase().indexOf(wrestler.first_name.toLowerCase()) >= 0){
			return true;
		} 
		console.log("NAME WARNING:: First Names didn't match::" + name + "/" + wrestler.first_name + " " + wrestler.last_name);
		return false;
	}
	static ParseLine(line: string, wrestler: IWrestler, pigTailCat: EPigTailType): IScore {
		var matchDesc = line.slice(0, line.indexOf(" - ")).trim();
		var rest = line.slice(line.indexOf(" - ") + 3);
		var decision = rest.slice(rest.indexOf("won") + 3, rest.indexOf("over")).trim();
		var winner = rest.slice(0, rest.indexOf("won")).trim();
		rest = rest.slice(rest.indexOf("over") + 4);
		var endPoint = rest.split("(", 2).join("(").length;
		var loser = rest.slice(0, endPoint).trim();
		var result = rest.slice(endPoint).trim();
		var advancementPoints = 0;
		var bonusPoints = 0;
		var placementPoints = 0;
		var decisionType = EDecisionType.DECISION;
		if (winner.indexOf(wrestler.last_name) >= 0) {
			if (winner.indexOf(wrestler.first_name) >=0 || winner.indexOf(wrestler.school) >= 0){
				var decisionStuff = this.GetDecisionType(decision, result);
				bonusPoints = decisionStuff.bonusPoints;
				decisionType = decisionStuff.decisionType
				var extraPoints = this.GetExtraPoints(matchDesc, pigTailCat);
				advancementPoints = extraPoints.advancementPoints;
				placementPoints = extraPoints.placementPoints;
			} else {
				return null;
			}
		}
		var totalPoints = advancementPoints + bonusPoints + placementPoints;
		var retVal: IScore = {
			matchDesc: matchDesc,
			winner: winner,
			decision: decision,
			loser: loser,
			decisionType: decisionType,
			result: result,
			teamPoints: totalPoints,
			advancementPoints: advancementPoints,
			bonusPoints: bonusPoints,
			placementPoints: placementPoints
		}

		return retVal;
	}
	static GetDecisionType(decision: string, result: string) {
		var retVal = { bonusPoints: 0, decisionType: EDecisionType.DECISION };
		if (decision.indexOf("major decision") >= 0) {
			retVal.bonusPoints = 1;
			retVal.decisionType = EDecisionType.MAJOR;
		} else if (decision.indexOf("tech fall") >= 0) {
			if (result.indexOf("TF-1.5") >= 0) {
				retVal.bonusPoints = 1.5;
				retVal.decisionType = EDecisionType.TECH_FALL_WITH_NEAR_FALL;
			} else {
				retVal.bonusPoints = 1;
				retVal.decisionType = EDecisionType.TECH_FALL;
			}
		} else if (decision.indexOf("fall") >= 0) {
			retVal.bonusPoints = 2;
			retVal.decisionType = EDecisionType.FALL;
		} else if (decision.indexOf("forfeit") >= 0) {
			retVal.bonusPoints = 2;
			retVal.decisionType = EDecisionType.FORFEIT;
		} else {
			retVal.decisionType = EDecisionType.DECISION;
		}
		return retVal;
	}

	static GetExtraPoints(matchDesc: string, pigTailCat: EPigTailType ) {
		var retVal = { advancementPoints: 0, placementPoints: 0};
		if (matchDesc.indexOf("Prelim") >= 0){
			if (pigTailCat == EPigTailType.CHAMP ){
				retVal.advancementPoints = 1;
			} else {
				retVal.advancementPoints = .5;				
			}
		} else if (matchDesc.indexOf("Champ. Round 1") >= 0 ){
			retVal.advancementPoints = 1;			
		} else if (matchDesc.indexOf("Champ. Round 2") >= 0){
			retVal.advancementPoints = 1;			
		} else if (matchDesc.indexOf("Quarterfinal") >= 0 ){
			retVal.advancementPoints = 1;			
			retVal.placementPoints = 6;
		} else if (matchDesc.indexOf("Semifinal") >= 0 ){
			retVal.advancementPoints = 1;			
			retVal.placementPoints = 6;
		} else if (matchDesc.indexOf("Cons. Round 1") >= 0 ){
			retVal.advancementPoints = .5;			
		} else if (matchDesc.indexOf("Cons. Round 2") >= 0 ){
			retVal.advancementPoints = .5;			
		} else if (matchDesc.indexOf("Cons. Round 3") >= 0 ){
			retVal.advancementPoints = .5;			
		} else if (matchDesc.indexOf("Cons. Round 4") >= 0 ){
			retVal.advancementPoints = .5;			
			retVal.placementPoints = 3;
		} else if (matchDesc.indexOf("Cons. Round 5") >= 0 ){
			retVal.advancementPoints = .5;			
			retVal.placementPoints = 3;
		} else if (matchDesc.indexOf("Cons. Semi") >= 0 ){
			retVal.advancementPoints = .5;			
			retVal.placementPoints = 3;
		} else if (matchDesc.indexOf("1st Place") >= 0 ){
			retVal.placementPoints = 4;
		} else if (matchDesc.indexOf("3rd Place") >= 0 ){
			retVal.placementPoints = 1;
		} else if (matchDesc.indexOf("5th Place") >= 0 ){
			retVal.placementPoints = 1;
		} else if (matchDesc.indexOf("7th Place") >= 0 ){
			retVal.placementPoints = 1;
		}

		return retVal;
	}
}


var allWrestlers = JSON.parse(fs.readFileSync('src/assets/data/participants.2017.json', 'utf8'));

var allWrestlersByWeight = {};

 _.forEach(allWrestlers, (data) => {
 	_.forEach(data, (wrestler, index) =>{
		wrestler.id = index;
 	} )

	_.forEach(weights, weight => {
		allWrestlersByWeight[weight] = _.filter(data, wrestler => wrestler.weight == weight);
	});
});


weights.forEach(weight=>{
	var wrestlers = allWrestlersByWeight[weight];
	var file = fs.readFileSync('src/assets/data/results.' + weight.toString() + '.txt', 'utf8');
	ScoreConvert.Convert(wrestlers, weight, file);
});

var output = JSON.stringify(allWrestlers);
fs.writeFileSync('src/assets/data/participants.2017.scores.json', output);
