import { AccountPage } from './league-pages/pages/account/account';
import { HomePage } from './league-pages/pages/home/home';
import { ELogin } from './store/models/login.enum';
import { AppState } from './store/wrestling.store';
import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';
import { Store } from '@ngrx/store';
import 'rxjs/add/operator/skipWhile';

import * as wrestling from './store/wrestlers.store/wrestlers.actions';
import * as fromRoot from './store/wrestling.store';

@Component({
	templateUrl: 'app.html'
})
export class MyApp {
	rootPage: any;

	constructor(
		private platform: Platform, 
		private store: Store<AppState>) {
		platform.ready().then(() => {
			// Okay, so the platform is ready and our plugins are available.
			// Here you can do any higher level native things you might need.
			StatusBar.styleDefault();
			Splashscreen.hide();
		});
		this.store.dispatch(new wrestling.LoadAction());
		
		this.store.select(fromRoot.getAuthenticatedState)
			.skipWhile(authState => authState === ELogin.NotSet)
			.first()
			.subscribe(authState => {
				if (authState === ELogin.NoUser)
					this.rootPage = HomePage;
				if (authState === ELogin.LoggedIn)
					this.rootPage = AccountPage;
			});
	}
}
