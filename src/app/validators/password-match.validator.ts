import { AbstractControl } from '@angular/forms';
export const passwordMatcher = (control: AbstractControl): { [key: string]: boolean } => {
	const password = control.get('password');
	const verify = control.get('verify');
	if (!password || !verify) return null;
	return password.value === verify.value ? null : { nomatch: true };
};