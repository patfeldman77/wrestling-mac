import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
	selector: 'full-row-button',
	templateUrl: 'full-row-button.html'
})
export class FullRowButtonComponent {
	@Input() buttonText: string;
	@Input() buttonIcon: string;
	@Input() buttonDisabled: boolean = false;

	@Output() buttonClick = new EventEmitter();

	constructor() { }

}
