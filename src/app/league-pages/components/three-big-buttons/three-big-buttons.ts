import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
	selector: 'three-big-buttons',
	templateUrl: 'three-big-buttons.html'
})
export class ThreeBigButtonsComponent {
	@Input() button1Text: string;
	@Input() button2Text: string;
	@Input() button3Text: string;
	@Input() helpButtonText: string;

	@Input() button1Icon: string;
	@Input() button2Icon: string;
	@Input() button3Icon: string;

	@Output() button1 = new EventEmitter();
	@Output() button2 = new EventEmitter();
	@Output() button3 = new EventEmitter();
	@Output() helpButton = new EventEmitter();
	constructor() { }

}
