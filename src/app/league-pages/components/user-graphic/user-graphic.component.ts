import { Component, Input } from '@angular/core';

@Component({
	selector: 'user-graphic',
	templateUrl: 'user-graphic.component.html'
})
export class UserGraphicComponent {
	@Input() letter1: string;
	@Input() letter2: string;
	@Input() colorVal: number = 1;

	constructor() {}

}
