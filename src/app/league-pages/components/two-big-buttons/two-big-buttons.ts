import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
	selector: 'two-big-buttons',
	templateUrl: 'two-big-buttons.html'
})
export class TwoBigButtonsComponent {
	@Input() bigButtonTopText: string;
	@Input() bigButtonBottomText: string;
	@Input() littleButtonLeftText: string;
	@Input() littleButtonRightText: string;
	@Input() bottomButtonText: string;
	@Input() helpButtonText: string;

	@Input() bigButtonTopIcon: string;
	@Input() bigButtonBottomIcon: string;
	@Input() littleButtonLeftIcon: string;
	@Input() littleButtonRightIcon: string;
	@Input() bottomButtonIcon: string;
	@Input() helpButtonIcon: string;

	@Output() bigButtonTopClick = new EventEmitter();
	@Output() bigButtonBottomClick = new EventEmitter();
	@Output() littleButtonLeftClick = new EventEmitter();
	@Output() littleButtonRightClick = new EventEmitter();
	@Output() bottomButtonClick = new EventEmitter();
	@Output() helpButton = new EventEmitter();
	constructor() {}
}
