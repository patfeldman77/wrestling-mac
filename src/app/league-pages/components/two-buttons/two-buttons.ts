import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
	selector: 'two-buttons',
	templateUrl: 'two-buttons.html'
})
export class TwoButtonsComponent {
	@Input() buttonLeftDisabled: boolean = false;
	@Input() buttonRightDisabled: boolean = false;
	@Input() buttonLeftText: string;
	@Input() buttonRightText: string;
	@Input() buttonLeftIcon: string;
	@Input() buttonRightIcon: string;

	@Output() buttonLeftClick = new EventEmitter();
	@Output() buttonRightClick = new EventEmitter();

	constructor() {
	}

}
