import { IUserLeagueRef } from './../../../store/models/user-league.interface';
import { FollowDraftModal } from './../../../draft-pages/pages-modal/follow-draft.modal/follow-draft.modal';
import { LoginModalPage } from './../../pages-modal/login-modal/login-modal';
import { AccountPage } from './../account/account';
import { JoinLeaguePage } from './../join-league/join-league';
import { CreateLeaguePage } from './../create-league/create-league';
import { ELogin } from './../../../store/models/login.enum';
import { Observable } from 'rxjs/Observable';
import { AppState } from './../../../store/wrestling.store';
import { Store } from '@ngrx/store';
import { Component } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';

import * as fromRoot from './../../../store/wrestling.store';
import * as userActions from './../../../store/user.store/user.actions';
import * as leaguesActions from './../../../store/leagues.store/leagues.actions';

@Component({
	selector: 'page-home',
	templateUrl: 'home.html'
})
export class HomePage {
	protected isAuth$: Observable<ELogin>;
	constructor(
		public navCtrl: NavController,
		public modalCtrl: ModalController,
		public _store: Store<AppState>) {
		this.isAuth$ = this._store.select(fromRoot.getAuthenticatedState);

	}

	protected Click_Create() {
		this.navCtrl.push(CreateLeaguePage);
	}

	protected Click_Login_Then_Create() {
		let modal = this.modalCtrl.create(LoginModalPage);
		modal.present();
		modal.onDidDismiss(data => {
			if (data === "Success") {
				this.navCtrl.push(CreateLeaguePage);
			}
		});
	}
	protected Click_Join() { this.navCtrl.push(JoinLeaguePage); }
	protected Click_Login_Then_Join() {
		let modal = this.modalCtrl.create(LoginModalPage);
		modal.present();
		modal.onDidDismiss(data => {
			if (data === "Success") {
				this.navCtrl.push(JoinLeaguePage);
			}
		});
	}

	protected Click_OpenMoeller() {
		let league: IUserLeagueRef = {
			leagueKey: '-Kf9gtqCIxwJEZ3gbGTI',
			isPrivate: true,
			leagueName: 'MoellerDraft',
			creator: 'Patrick Feldman',
			creatorId: 'RnHonPsfuSfzBGLU0Ocqcqd1G0A3',
			draftDate: 1489464000000
		}
		// let league: IUserLeagueRef = {
		// 	creator: 'Patrick Feldman',
		// 	creatorId: 'RnHonPsfuSfzBGLU0Ocqcqd1G0A3',
		// 	draftDate: 1489464000000,
		// 	isPrivate: true,
		// 	leagueKey: "-KfDIYYatmZbHaBKHaMd",
		// 	leagueName: "FeldmanPlus"
		// }
		this._store.dispatch(new leaguesActions.SetCurrentLeagueAction(league));
		this.navCtrl.setRoot(FollowDraftModal);
	}

	protected Click_Donate() { alert("Coming soon: Maybe some pizza for now :)"); }
	protected Click_Logout() { this._store.dispatch(new userActions.LogoutUserAction()); }
	protected Click_Login() { this.modalCtrl.create(LoginModalPage).present(); }
	protected Click_Dashboard() { this.navCtrl.setRoot(AccountPage); }
}
