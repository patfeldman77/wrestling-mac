import { IDraftUser } from './../../../store/models/user.interface';
import { AddParticipantModal } from './../../pages-modal/add-participant.modal/add-participant.modal';
import { AccountPage } from './../account/account';
import { AppState } from './../../../store/wrestling.store';
import { Store } from '@ngrx/store';
import { ILeague } from './../../../store/models/league.interface';
import { Observable } from 'rxjs/Observable';
import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, AlertController } from 'ionic-angular';
import { DragulaService } from "ng2-dragula";

import * as fromRoot from '../../../store/wrestling.store';
import * as fromDraft from '../../../store/draft.store/draft.reducer';
import * as draftActions from '../../../store/draft.store/draft.actions';
import * as _ from 'lodash';

@Component({
	selector: 'league-admin',
	templateUrl: 'league-admin.page.html'
})
export class LeagueAdminPage {
	protected $currentLeague: Observable<ILeague>;
	private $draftData: Observable<fromDraft.State>;
	private draftUsers: IDraftUser[];
	private $myId: Observable<string>;
	private isAdmin: boolean;
	private myDate: string;

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		public modalCtrl: ModalController,
		public alertCtrl: AlertController,
		public dragService: DragulaService,
		public _store: Store<AppState>) {

		dragService.drop.subscribe(() => {
			this.draftUsers.forEach((user: IDraftUser, index) => {
				user.draftOrder = index;
			})
			this._store.dispatch(new draftActions.UpdateDraftOrderAction(this.draftUsers));
		});

		this.$myId = this._store.select(fromRoot.getMyId);
		this._store.select(fromRoot.isCurrentLeagueAdmin).take(1).subscribe(isAdmin => {
			this.isAdmin = isAdmin;
			this.$currentLeague = this._store.select(fromRoot.getCurrentLeague).skipWhile(league => !league).take(1);
			this.$draftData = this._store.select(fromRoot.getDraft);
			this._store.select(fromRoot.getDraftUsers).subscribe(users => {
				this.draftUsers = _.map(users, (value, key) => value).sort((a, b) => a.draftOrder - b.draftOrder);
			});
			this.$draftData.skipWhile(data => !data.draftSetup.draftDate).take(1).subscribe(data => {
				let date = new Date(data.draftSetup.draftDate);

				// console.log(data.draftSetup.draftDate);
				// console.log(date.toISOString());
				// console.log(Date.now());
				this.myDate = date.toISOString();
				//this.myDate ="1990-02-19 07:43";//date.toDateString();
			})
		})
	}

	protected Change_Date(dateIsoString: string){
		let date = new Date (dateIsoString);
		this._store.dispatch(new draftActions.UpdateDraftTimeAction(date.getTime()));

		//console.log(date.getTime());
	}
	protected Click_DeleteParticipant(user: IDraftUser) {
		let alert = this.alertCtrl.create({
			title: 'Confirm Delete',
			message: 'Are you sure you want to delete ' + user.name + '?',
			buttons: [
				{
					text: 'Cancel',
					role: 'cancel',
				},
				{
					text: 'Delete',
					handler: () => {
						this._store.dispatch(new draftActions.DeleteParticipantFromDraftAction(user));
					}
				}
			]
		});
		alert.present();
	}
	protected Click_AddParticipant() {
		let modal = this.modalCtrl.create(AddParticipantModal);
		modal.present();
		modal.onDidDismiss(data => {
			if (!!data) {
				this._store.dispatch(new draftActions.AddParticipantToDraftAction(data.name));
			}
		});
	}

	protected Click_Close() {
		// Save date, draft order, and participants
		this.navCtrl.setRoot(AccountPage);
	}
}
