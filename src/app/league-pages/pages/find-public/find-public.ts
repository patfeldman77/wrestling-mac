import { AccountPage } from './../account/account';
import { CreatePublicPage } from './../create-public/create-public';
import { IUserWithKey } from './../../../store/models/user.interface';
import { ILeague } from './../../../store/models/league.interface';
import { AppState } from './../../../store/wrestling.store';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, Loading, LoadingController } from 'ionic-angular';
import * as leagues from './../../../store/leagues.store/leagues.actions';
import * as fromRoot from './../../../store/wrestling.store';

@Component({
	selector: 'page-find-public',
	templateUrl: 'find-public.html'
})
export class FindPublicPage {
	private $leagues: Observable<ILeague[]>;	
	private $myInfo: Observable<IUserWithKey>;
	protected loader: Loading;

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		public loadingCtrl: LoadingController,
		public modalCtrl: ModalController,
		public _store: Store<AppState>) {
		this.$leagues = this._store.select(fromRoot.getPublicLeagues);
		this.$myInfo = this._store.select(fromRoot.getMyInfo).take(1);
		this._store.dispatch(new leagues.PublicSearchAction(''));
	}

	protected Input_FindLeagues($event){
		console.log($event);
	}
	protected Click_CreateLeague(){
		this.navCtrl.push(CreatePublicPage);
	}
	protected Click_JoinLeague(league:ILeague){
		this._store.dispatch(new leagues.JoinLeagueAction({
			leagueName: league.name, 
			leagueKey: league.$key, 
			leagueDraftDate: league.draftSetup.draftDate,
			leaguePassword: null,
			leagueCreator: league.creator,
			leagueCreatorId: league.creatorId,
			isPrivate: false
		}));
		this.navCtrl.push(AccountPage);
	}

	protected Click_OpenLeague(league:ILeague){
		// this._store.dispatch(new leagues.SetCurrentLeagueAction(league));
		// this.navCtrl.setRoot(TabsPage);
	}

	protected Click_ShareLeague(league: ILeague){

	}
}
