import { FindPublicPage } from './../find-public/find-public';
import { FindPrivatePage } from './../find-private/find-private';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
@Component({
	selector: 'page-join-league',
	templateUrl: 'join-league.html'
})
export class JoinLeaguePage {

	constructor(public navCtrl: NavController, public navParams: NavParams) { }
	protected Click_Private() { 
		this.navCtrl.push(FindPrivatePage); 
	}
	protected Click_Public() { 
		this.navCtrl.push(FindPublicPage); 
	}
	protected Click_Questions() { alert("Help: Coming soon"); }

}
