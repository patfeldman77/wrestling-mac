import { CreatePrivatePage } from './../create-private/create-private';
import { CreatePublicPage } from './../create-public/create-public';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
	selector: 'page-create-league',
	templateUrl: 'create-league.html'
})
export class CreateLeaguePage {
	constructor(public navCtrl: NavController, public navParams: NavParams) { }

	protected Click_Private() { this.navCtrl.push(CreatePrivatePage); }
	protected Click_Public() { this.navCtrl.push(CreatePublicPage); }
	protected Click_Questions() { alert("Help: Coming Soon"); }
}
