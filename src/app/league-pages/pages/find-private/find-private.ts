import { AccountPage } from './../account/account';
import { AppState } from './../../../store/wrestling.store';
import { eJoinStatus } from './../../../store/models/league-join.interface';
import { Store } from '@ngrx/store';
import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController, Loading } from 'ionic-angular';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import * as leagues from './../../../store/leagues.store/leagues.actions';
import * as fromRoot from '../../../store/wrestling.store';

@Component({
	selector: 'page-find-private',
	templateUrl: 'find-private.html'
})
export class FindPrivatePage {
	protected form: FormGroup;
	protected errorText: string;
	protected errorText2: string;
	protected loader: Loading;
	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		public loadingCtrl: LoadingController,
		public toastCtrl: ToastController,
		public fb: FormBuilder, 
		public _store: Store<AppState>) {

		this.form = fb.group({
			"leagueName": ["", Validators.required],
			"leaguePassword": ["", Validators.required]
		});
	}

	protected Click_Register() {
		this.errorText = null;
		let leagueName = this.form.value.leagueName;
		let leaguePassword = this.form.value.leaguePassword;
		this._store.dispatch(new leagues.JoinLeagueAction({
			leagueName: leagueName, 
			leaguePassword: leaguePassword,
			leagueDraftDate: null,
			leagueKey: null,
			leagueCreator: null, 
			leagueCreatorId: null,
			isPrivate: true
		}));
		this.loader = this.loadingCtrl.create({
			content: 'Finding League : ' + leagueName + '...'
		});
		this.loader.present();

		this._store.select(fromRoot.getJoinLeagueStatus)
			.skipWhile(status => status.status === eJoinStatus.Joining)
			.take(1)
			.subscribe(status => {
			switch (status.status){
				case eJoinStatus.Nothing:
					console.log("NOTHING??"); 
					break;
				case eJoinStatus.Joining:
					console.log("Joining??"); 
					break;
				case eJoinStatus.Joined:
					if (!!this.loader)
						this.loader.dismiss();
					this.navCtrl.push(AccountPage);
					break;
				case eJoinStatus.Error:
					if (!!this.loader)
						this.loader.dismiss();
					this.errorText = '** League could not be found or Password was incorrect **';
					this.errorText2 = 'Please try again.';
					break;
			}
		});

	}
}
