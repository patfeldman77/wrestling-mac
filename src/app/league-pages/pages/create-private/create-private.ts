import { AccountPage } from './../account/account';
import { passwordMatcher } from './../../../validators/password-match.validator';
import { AppState } from './../../../store/wrestling.store';
import { Store } from '@ngrx/store';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import * as leagues from './../../../store/leagues.store/leagues.actions';

@Component({
	selector: 'page-create-private',
	templateUrl: 'create-private.html'
})
export class CreatePrivatePage {
	protected createForm: FormGroup;
	protected errorText: string;
	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		public fb: FormBuilder,
		public _store: Store<AppState>
	) {
		this.createForm = fb.group({
			'leagueName': ['', Validators.compose([Validators.required, Validators.minLength(8)])],
			'password': ['', Validators.compose([Validators.required, Validators.minLength(8)])],
			'verify': ['', Validators.compose([Validators.required])],
		}, { validator: passwordMatcher });

		this.createForm.valueChanges.subscribe(data => this.onValueChanged(data));
		this.onValueChanged(); // (re)set validation messages now
	}

	protected Click_CreateLeague() {
		this._store.dispatch(new leagues.CreatePrivateLeagueAction({
			name: this.createForm.value.leagueName,
			password: this.createForm.value.password,
			maxUsers: 100,
			createDate: Date.now(),
			draftDate: Date.now()
		}));
		this.navCtrl.push(AccountPage);
	}


	onValueChanged(data?: any) {
		if (!this.createForm) { return; }
		const form = this.createForm;
		let controlsValid = true;
		for (const field in this.formErrors) {
			// clear previous error message (if any)
			this.formErrors[field] = '';
			const control = form.get(field);
			if (control && control.dirty && !control.valid) {
				const messages = this.validationMessages[field];
				for (const key in control.errors) {
					this.formErrors[field] += messages[key] + ' ';
				}
				controlsValid = false;
			}
		}
		if (controlsValid && !!this.createForm.errors && this.createForm.get('verify').value.length > 0){
			this.formErrors['verify'] = this.validationMessages.form.passwordMatcher;
		}
	}

	formErrors = {
		'leagueName': '',
		'password': '',
		'verify': ''
	};
	validationMessages = {
		'leagueName': {
			'required': 'Name is required',
			'minlength': 'Minimum 8 characters long',
		},
		'password': {
			'required': 'Password is required',
			'minlength': 'Minimum 8 characters long',
		},
		'verify': {
			'required': 'Password is required',
		}, 
		'form': {
			'passwordMatcher': 'Passwords must match'
		}
	};

}
