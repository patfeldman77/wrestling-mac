import { AccountPage } from './../account/account';
import { AppState } from './../../../store/wrestling.store';
import { Store } from '@ngrx/store';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import * as leagues from './../../../store/leagues.store/leagues.actions';

@Component({
	selector: 'page-create-public',
	templateUrl: 'create-public.html'
})
export class CreatePublicPage {
	protected createForm: FormGroup;

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		public fb: FormBuilder,
		public _store: Store<AppState>) {

		this.createForm = fb.group({
			'leagueName': ['', Validators.compose([Validators.required, Validators.minLength(8)])],
			'leagueMaxMembers': ['10']
		});
		this.createForm.valueChanges.subscribe(data => this.onValueChanged(data));
		this.onValueChanged(); // (re)set validation messages now
	}

	protected Click_CreateLeague(){
		this._store.dispatch(new leagues.CreatePublicLeagueAction({
			name: this.createForm.value.leagueName,
			password: '',
			maxUsers: this.createForm.value.leagueMaxMembers,
			createDate: Date.now(),
			draftDate: Date.now()
		}));
		this.navCtrl.push(AccountPage);
	}

	onValueChanged(data?: any) {
		if (!this.createForm) { return; }
		const form = this.createForm;
		let controlsValid = true;
		for (const field in this.formErrors) {
			// clear previous error message (if any)
			this.formErrors[field] = '';
			const control = form.get(field);
			if (control && control.dirty && !control.valid) {
				const messages = this.validationMessages[field];
				for (const key in control.errors) {
					this.formErrors[field] += messages[key] + ' ';
				}
				controlsValid = false;
			}
		}
	}

	formErrors = {
		'leagueName': ''
	};
	validationMessages = {
		'leagueName': {
			'required': 'Name is required',
			'minlength': 'Minimum 8 characters long',
		}
	};



}
