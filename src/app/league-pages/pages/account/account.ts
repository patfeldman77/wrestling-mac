import { LeagueAdminPage } from '../league-admin.page/league-admin.page';
import { HomePage } from './../home/home';
import { TabsPage } from './../../../draft-pages/pages/tabs/tabs';
import { AppState } from './../../../store/wrestling.store';
import { IUser } from './../../../store/models/user.interface';
import { IUserLeagueRef } from './../../../store/models/user-league.interface';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import * as leagueActions from './../../../store/leagues.store/leagues.actions';
import * as userActions from './../../../store/user.store/user.actions';
import * as draftActions from './../../../store/draft.store/draft.actions';
import * as fromRoot from '../../../store/wrestling.store';

@Component({
	selector: 'page-account',
	templateUrl: 'account.html'
})
export class AccountPage {
	protected $leagues: Observable<dictionary<IUserLeagueRef>>;
	protected $myInfo: Observable<IUser>;

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		public _store: Store<AppState>) {

	}
	ionViewWillEnter(){
		this.$myInfo = this._store.select(fromRoot.getMyInfo);
		// ORDER MATTERS HERE
		this._store.dispatch(new leagueActions.ResetCurrentLeagueAction());
		this._store.dispatch(new draftActions.UnsubscribeToDraftAction());
	}
	ionViewWillLeave(){
		
	}

	public Click_OpenLeague(league: IUserLeagueRef){
		this._store.dispatch(new leagueActions.SetCurrentLeagueAction(league));
		this.navCtrl.setRoot(TabsPage);
	}
	public Click_JoinLeague() {
		this.navCtrl.setRoot(HomePage);
	}

	public Click_Logout() {
		this.navCtrl.setRoot(HomePage);
		this._store.dispatch(new userActions.LogoutUserAction());
	}

	public Click_OpenAdmin(league: IUserLeagueRef){
		this._store.dispatch(new leagueActions.SetCurrentLeagueAction(league));
		this.navCtrl.setRoot(LeagueAdminPage);
	}

	public Click_Donate(){
		alert('Save a beer for me');
	}
}
