import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
@Component({
	selector: 'add-participant',
	templateUrl: 'add-participant.modal.html'
})
export class AddParticipantModal {
	protected createForm: FormGroup;
	protected errorText: string;

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		public viewCtrl: ViewController,
		public fb: FormBuilder) {
		this.createForm = fb.group({
			'name': ['', Validators.compose([Validators.required, Validators.minLength(3)])],
		});
		this.createForm.valueChanges.subscribe(data => this.onValueChanged(data));
		this.onValueChanged(); // (re)set validation messages now
	}

	protected Click_Close() {
		this.viewCtrl.dismiss();
	}

	protected Click_AddParticipant() {
		this.viewCtrl.dismiss({name: this.createForm.value.name
		});
	}

	protected onValueChanged(data?: any) {
		if (!this.createForm) { return; }
		const form = this.createForm;
		let controlsValid = true;
		for (const field in this.formErrors) {
			// clear previous error message (if any)
			this.formErrors[field] = '';
			const control = form.get(field);
			if (control && control.dirty && !control.valid) {
				const messages = this.validationMessages[field];
				for (const key in control.errors) {
					this.formErrors[field] += messages[key] + ' ';
				}
				controlsValid = false;
			}
		}
	}

	formErrors = {
		'name': ''
	};
	validationMessages = {
		'name': {
			'required': 'Name is required',
			'minlength': 'Minimum 3 characters long',
		}
	};
}
