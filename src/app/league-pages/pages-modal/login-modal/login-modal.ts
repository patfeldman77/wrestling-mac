import { Store } from '@ngrx/store';
import { AppState } from './../../../store/wrestling.store';
import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { ELogin } from './../../../store/models/login.enum';
import * as fromRoot from '../../../store/wrestling.store';
import * as userActions from '../../../store/user.store/user.actions';
@Component({
	selector: 'page-login-modal',
	templateUrl: 'login-modal.html'
})
export class LoginModalPage {

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		public _store: Store<AppState>,
		public viewCtrl: ViewController) {
		this._store.select(fromRoot.getAuthenticatedState)
			.skipWhile(authState => authState !== ELogin.LoggedIn)
			.first()
			.subscribe(authState => {
				if (authState === ELogin.LoggedIn) this.viewCtrl.dismiss("Success");
			});
	}

	protected Click_Facebook() {
		this._store.dispatch(new userActions.LoginUserFacebookAction());
	}
	protected Click_Google() {
		this._store.dispatch(new userActions.LoginUserGoogleAction());
	}
	protected Click_Twitter() {
		alert("Sorry, not here yet, coming soon!");
	}
	protected Click_Close() {
		this.viewCtrl.dismiss("Closed");
	}

}
