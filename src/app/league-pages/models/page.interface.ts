import { Component } from '@angular/core';

export interface IPage{
	name: string;
	longName?: string;
	component: Component; 
	titleLogo?:string;
	logo?: string;
	isRoot?: boolean;
	children?: IPage[];
}