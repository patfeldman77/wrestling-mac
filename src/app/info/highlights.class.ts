export interface IWrestlerHighlight {
	highlightId: number, 
	priority: number, 
	wrestlerId: number, 
	wrestlerWeight: string,
	key: string
}

export interface IHighlightDetails {
	name : string, 
	color : string, 
	textColor: string, 
	icon: string, 
	id: number
}
export const defaultHighlights:IHighlightDetails[] = [
	{
		'name' : 'Draft List',
		'color' : '#1976D2', 
		'textColor' : '#eee',
		'icon' : 'list', 
		'id': 0
	},
	{
		'name' : 'Diamond In The Rough',
		'color' : '#303F9F',
		'textColor' : '#eee',
		'icon' : 'star',
		'id': 1
	},
	{
		'name' : 'Really, nothing better?',
		'color' : '#E64A19',
		'textColor' : '#eee',
		'icon' : 'help',
		'id': 2
	},
	{
		'name' : 'Danger, watch yourself!',
		'color' : '#F9A825', 
		'textColor' : '#111',
		'icon' : 'nuclear',
		'id': 3
	},	
]