import { OrderByScorePipe } from './pipes/team-order-by-score.pipe';
import { ReverseMatchOrderPipe } from './pipes/reverse-match-order.pipe';
import { RoundToStringPipe } from './pipes/round-to-string.pipe';
import { StripParensPipe } from './pipes/strip-parens.pipe';
import { JustNamePipe } from './pipes/just-name.pipe';
import { TotalTeamScorePipe } from './pipes/total-team-score.pipe';
import { TotalScorePipe } from './pipes/total-score.pipe';
import { ResultsPage } from './draft-pages/pages/results.page/results.page';
import { WeightModal } from './draft-pages/pages-modal/weight.modal/weight.modal';
import { FollowDraftModal } from './draft-pages/pages-modal/follow-draft.modal/follow-draft.modal';
import { WrestlerListByWeightComponent } from './draft-pages/components/wrestler-list-by-weight.component/wrestler-list-by-weight.component';
import { WrestlerListComponent } from './draft-pages/components/wrestler-list.component/wrestler-list.component';
import { CharAtPipe } from './pipes/char-at.pipe';
import { MyTeamPage } from './draft-pages/pages/my-team.page/my-team.page';
import { HighlightTextPipe } from './pipes/highlight-text.pipe';
import { HighlightBgPipe } from './pipes/highlight-bg.pipe';
import { HighlightIconPipe } from './pipes/highlight-icon.pipe';
import { HighlightModalPage } from './draft-pages/pages-modal/highlight.modal/highlight.modal';
import { OrdinalPipe } from './pipes/ordinal.pipe';
import { AddParticipantModal } from './league-pages/pages-modal/add-participant.modal/add-participant.modal';
import { LoginModalPage } from './league-pages/pages-modal/login-modal/login-modal';
import { WrestlersIdsToInfoPipe } from './pipes/wrestlers-ids-to-info.pipe';
import { WrestlerIdToNamePipe } from './pipes/wrestler-id-to-name.pipe';
import { LeagueTeamByKeyPipe } from './pipes/league-team-by-key.pipe';
import { UserGraphicComponent } from './league-pages/components/user-graphic/user-graphic.component';
import { TimeRemainingInHoursPipe } from './pipes/time-remaining.pipe';
import { FullRowButtonComponent } from './league-pages/components/full-row-button/full-row-button';
import { TwoButtonsComponent } from './league-pages/components/two-buttons/two-buttons';
import { ThreeBigButtonsComponent } from './league-pages/components/three-big-buttons/three-big-buttons';
import { TwoBigButtonsComponent } from './league-pages/components/two-big-buttons/two-big-buttons';
import { LeaguePage } from './draft-pages/pages/league.page/league.page';
import { WrestlersPage } from './draft-pages/pages/wrestlers-page/wrestlers-page';
import { WrestlerModalPage } from './draft-pages/pages-modal/wrestler-modal/wrestler-modal';
import { TabsPage } from './draft-pages/pages/tabs/tabs';
import { DraftHeaderComponent } from './draft-pages/components/draft-header.component/draft-header.component';
import { SortByDraftOrderPipe } from './pipes/sort-by-draft-order.pipe';
import { IsDraftingPipe } from './pipes/is-drafting.pipe';
import { EmailPrunePipe } from './pipes/email-prune.pipe';
import { OrderByDraftPipe } from './pipes/order-by-draft.pipe';
import { ValuesPipe } from './pipes/values.pipe';
import { KeysPipe } from './pipes/keys.pipe';
import { IsLeagueMemberPipe } from './pipes/is-league-member.pipe';
import { WrestlersByWeightPipe } from './pipes/wrestlers-by-weight.pipe';
import { IsLoggedInPipe } from './pipes/is-logged-in.pipe';
import { AccountPage } from './league-pages/pages/account/account';
import { WrestlingService } from './store/providers/wrestling.api';
import { CreatePublicPage } from './league-pages/pages/create-public/create-public';
import { CreatePrivatePage } from './league-pages/pages/create-private/create-private';
import { firebaseConfig } from './firebase.config';
import { FindPublicPage } from './league-pages/pages/find-public/find-public';
import { FindPrivatePage } from './league-pages/pages/find-private/find-private';
import { DonatePage } from './league-pages/pages/donate/donate';
import { CreateLeaguePage } from './league-pages/pages/create-league/create-league';
import { JoinLeaguePage } from './league-pages/pages/join-league/join-league';
import { NgModule, ErrorHandler } from '@angular/core';
import { ReactiveFormsModule } from "@angular/forms";
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from './league-pages/pages/home/home';
import { AngularFireModule } from 'angularfire2';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { StoreLogMonitorModule } from '@ngrx/store-log-monitor';
import { StoreModule } from '@ngrx/store';
import { DragulaModule } from 'ng2-dragula';
import * as wrestlingStore from './store/wrestling.store';
import { LeagueAdminPage } from "./league-pages/pages/league-admin.page/league-admin.page";

@NgModule({
	declarations: [
		// App Base
		// TEST string
		MyApp,

		// Pipes
		IsLoggedInPipe,
		IsLeagueMemberPipe,
		KeysPipe,
		ValuesPipe,
		WrestlersByWeightPipe,
		OrderByDraftPipe,
		EmailPrunePipe,
		IsDraftingPipe,
		SortByDraftOrderPipe,
		TimeRemainingInHoursPipe,
		LeagueTeamByKeyPipe,
		WrestlerIdToNamePipe,
		WrestlersIdsToInfoPipe,
		OrdinalPipe,
		HighlightIconPipe,
		HighlightBgPipe, 
		HighlightTextPipe,
		CharAtPipe,
		TotalScorePipe,
		TotalTeamScorePipe,
		JustNamePipe, 
		StripParensPipe,
		RoundToStringPipe,
		ReverseMatchOrderPipe,
		OrderByScorePipe,

		// Pages 
		AccountPage,
		HomePage,
		TabsPage,
		JoinLeaguePage,
		CreateLeaguePage,
		DonatePage,
		FindPrivatePage,
		FindPublicPage,
		CreatePrivatePage,
		CreatePublicPage,
		WrestlersPage,
		MyTeamPage,
		LeaguePage,
		LeagueAdminPage,
		ResultsPage,

		// MODALS
		LoginModalPage,
		WrestlerModalPage,
		AddParticipantModal,
		HighlightModalPage,
		FollowDraftModal,
		WeightModal,

		// Components
		TwoBigButtonsComponent,
		ThreeBigButtonsComponent,
		TwoButtonsComponent,
		FullRowButtonComponent,
		DraftHeaderComponent, 
		UserGraphicComponent,
		WrestlerListComponent, 
		WrestlerListByWeightComponent
	],
	imports: [
		// store info
		StoreModule.provideStore(wrestlingStore.reducer),
		StoreDevtoolsModule.instrumentOnlyWithExtension(),
		StoreLogMonitorModule,

		// Effects
		...wrestlingStore.RunEffects,

		// angular fire module
		AngularFireModule.initializeApp(firebaseConfig),
		IonicModule.forRoot(MyApp),
		ReactiveFormsModule, 
		DragulaModule
	],
	bootstrap: [IonicApp],
	entryComponents: [
		MyApp,

		//Pages are the entry components
		AccountPage,
		HomePage,
		TabsPage,
		JoinLeaguePage,
		CreateLeaguePage,
		DonatePage,
		FindPrivatePage,
		FindPublicPage,
		CreatePrivatePage,
		CreatePublicPage,
		WrestlersPage,
		MyTeamPage,
		LeaguePage,
		LeagueAdminPage,
		ResultsPage,

		// MODALS
		LoginModalPage, 
		WrestlerModalPage,
		AddParticipantModal,
		HighlightModalPage,
		FollowDraftModal,
		WeightModal


	],
	providers: [
		WrestlingService,
		{ provide: ErrorHandler, useClass: IonicErrorHandler }
	]
})
export class AppModule { }
