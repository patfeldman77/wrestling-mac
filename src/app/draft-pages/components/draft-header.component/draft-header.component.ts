import { IDraftUser } from './../../../store/models/user.interface';
import { Component, Input, OnChanges } from '@angular/core';

@Component({
	selector: 'draft-header',
	templateUrl: 'draft-header.component.html'
})
export class DraftHeaderComponent implements OnChanges {
	@Input() leagueName: string;
	@Input() users: dictionary<IDraftUser>;
	@Input() myId: string;
	@Input() isDrafting: boolean;
	@Input() isDraftComplete: boolean = false;
	@Input() timeTillDraftInMS: number;
	@Input() numberDrafted: number;
	@Input() currentRound: number;

	constructor() {
	}

	ngOnChanges(){
	}
}
