import { weights } from './../../../info/weights';
import { IWrestler } from './../../../store/models/wrestlers.interface';
import { Component, Input, Output, EventEmitter, OnChanges } from '@angular/core';

@Component({
	selector: 'wrestler-list-by-weight',
	templateUrl: 'wrestler-list-by-weight.component.html'
})
export class WrestlerListByWeightComponent implements OnChanges {
	@Input() team: IWrestler[] = [];
	@Input() title: string;
	@Input() bgColorSlot: number = null;
	@Input() icon: string = null;
	@Input() titleTextColor: string=null;
	@Input() titleBgColor: string;
	@Output() wrestlerSelect = new EventEmitter();
	@Output() weightSelect = new EventEmitter();
	protected weights = weights;
	protected teamByWeight: dictionary<IWrestler> = {};
	constructor() {
		this.weights.forEach(weight =>{
			this.teamByWeight[weight] = null;
		})
		this.teamByWeight['extra'] = null;

		this.processTeam();
	
	}

	ngOnChanges(){
		this.processTeam();
	}

	protected processTeam(){
		if (!this.team || this.team.length === 0) return;
		this.weights.forEach(weight =>{
			let allAtWeight = this.team.filter(wrestler => wrestler.weight == weight).sort( (a,b) => a.rank-b.rank);
			if (allAtWeight.length > 1){
				this.teamByWeight['extra'] = allAtWeight[1];
			}
			
			if (allAtWeight.length > 0){
				this.teamByWeight[weight] = allAtWeight[0];
			}
		});
	}
}
