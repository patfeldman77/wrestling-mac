import { weights } from './../../../info/weights';
import { IWrestler } from './../../../store/models/wrestlers.interface';
import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
	selector: 'wrestler-list',
	templateUrl: 'wrestler-list.component.html'
})
export class WrestlerListComponent {
	@Input() team: IWrestler[];
	@Input() title: string;
	@Input() bgColorSlot: number = null;
	@Input() icon: string = null;
	@Input() titleTextColor: string=null;
	@Input() titleBgColor: string;
	@Output() wrestlerSelect = new EventEmitter()

	constructor() {
	}
}
