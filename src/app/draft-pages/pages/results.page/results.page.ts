import { WeightModal } from './../../pages-modal/weight.modal/weight.modal';
import { WrestlerModalPage } from './../../pages-modal/wrestler-modal/wrestler-modal';
import { IWrestler } from './../../../store/models/wrestlers.interface';
import { IDraftUser } from './../../../store/models/user.interface';
import { ILeague, IDraftedWrestler } from './../../../store/models/league.interface';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { AppState } from './../../../store/wrestling.store';
import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ModalController } from 'ionic-angular';

import * as fromRoot from '../../../store/wrestling.store';
import * as fromDraft from '../../../store/draft.store/draft.reducer';

@Component({
	selector: 'page-results',
	templateUrl: 'results.page.html'
})
export class ResultsPage {
	private $currentLeague: Observable<ILeague>;
	private $draftData: Observable<fromDraft.State>;
	private $liveDraftedTeams: Observable<dictionary<IDraftedWrestler[]>>;
	private $draftUsers: Observable<dictionary<IDraftUser>>;

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		public alertCtrl: AlertController,
		public modalCtrl: ModalController,
		public _store: Store<AppState>) {
		this.$currentLeague = this._store.select(fromRoot.getCurrentLeague).skipWhile(league => !league).take(1);
		this.$draftData = this._store.select(fromRoot.getDraft).take(1);
		this.$liveDraftedTeams = this._store.select(fromRoot.getLiveDraftedTeams);
		this.$draftUsers = this._store.select(fromRoot.getDraftUsers);
	}

	public Expectations() {
		let alert = this.alertCtrl.create({
			title: 'What do you want from me!',
			message: 'I ain\'t got no time for no jibba jabba!',
			buttons: [{ text: 'COME ON!', role: 'cancel' }]
		});
		alert.present();

	}
	Click_OpenWrestler(wrestler: IWrestler) {
		let wrestlerModal = this.modalCtrl.create(WrestlerModalPage, { wrestler: wrestler });
		wrestlerModal.present();
	}

	Click_OpenWeight(weight: string) {
		let wrestlerModal = this.modalCtrl.create(WeightModal, { weight: weight });
		wrestlerModal.present();
	}

}
