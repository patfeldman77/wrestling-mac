import { WeightModal } from './../../pages-modal/weight.modal/weight.modal';
import { IWrestlerHighlight } from './../../../info/highlights.class';
import { weights } from './../../../info/weights';
import { ISubscription } from 'rxjs/Subscription';
import { IDraftedWrestler } from './../../../store/models/league.interface';
import { WrestlerModalPage } from './../../pages-modal/wrestler-modal/wrestler-modal';
import { IWrestler } from './../../../store/models/wrestlers.interface';
import { AppState } from './../../../store/wrestling.store';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import * as _ from 'lodash';
import * as fromRoot from '../../../store/wrestling.store';

const DEFAULT_SHOW_NUM = 16;
@Component({
	selector: 'page-wrestlers',
	templateUrl: 'wrestlers-page.html'
})
export class WrestlersPage {
	private $isAdmin: Observable<boolean>;
	private $isDraftComplete: Observable<boolean>;
	private allWeights: string[];
	private allWrestlersByWeight: dictionary<IWrestler[]> = {};
	private showWrestlersByWeight: dictionary<IWrestler[]> = {};
	private knownDrafts: IDraftedWrestler[] = [];
	private liveDraftSubscription$: ISubscription;
	private currentLeagueSubscription$: ISubscription;
	private currentTeamSubscription$: ISubscription;
	private myHighlights$: ISubscription;
	private knownHighlights: IWrestlerHighlight[];
	private myTeam: dictionary<IWrestler> = {};
	private currentTeam: dictionary<IDraftedWrestler> = {};
	private isShowing: dictionary<boolean> = {}
	private isDrafting$: Observable<boolean>;
	private isMyTurn$: Observable<boolean>;
	private myId: string;

	private isHidingMyWeight: boolean = false;
	private isAdminHide: boolean = false;
	private isHidingSelected: boolean = false;

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		public modalCtrl: ModalController,
		public _store: Store<AppState>) { }

	ionViewDidLoad() {
		this.currentTeamSubscription$ = this._store.select(fromRoot.getCurrentDraftTeam).subscribe(draftedTeam => {
			if (!draftedTeam) return;
			this.currentTeam = {};
			draftedTeam.forEach(wrestler =>{
				this.currentTeam[wrestler.wrestlerWeight] = wrestler; 
			});
		})
		this.$isAdmin = this._store.select(fromRoot.isCurrentLeagueAdmin).take(1);
		this.$isDraftComplete = this._store.select(fromRoot.isDraftComplete).take(1);
		this._store.select(fromRoot.getMyId).take(1).subscribe(id => {
			this.myId = id;
		})
		this._store.select(fromRoot.getWrestlers).skipWhile(wrestlersState => !wrestlersState.wrestlersLoaded).take(1).subscribe(wrestlersState => {
			this.allWeights = weights;
			let allWrestlers = _.cloneDeep(wrestlersState.wrestlers.wrestlersById);
			this.allWeights.forEach(weight => {
				this.allWrestlersByWeight[weight] = _.map(allWrestlers, (value, key) => value)
					.filter(wrestler => {
						return !!wrestler && wrestler.weight == weight;
					}).sort((a: IWrestler, b: IWrestler) => a.rank - b.rank);
			});
			this.initShowWrestlersByWeight();
		});
		this.isDrafting$ = this._store.select(fromRoot.isDrafting);
		this.isMyTurn$ = this._store.select(fromRoot.isMyTurn);
		this.currentLeagueSubscription$ = this._store.select(fromRoot.getCurrentLeague).subscribe(league => {
			if (!league && !!this.liveDraftSubscription$) {
				this.liveDraftSubscription$.unsubscribe();
				this.currentLeagueSubscription$.unsubscribe();
				this.myHighlights$.unsubscribe();
				this.currentTeamSubscription$.unsubscribe();
			}
		});
		this.myHighlights$ = this._store
			.select(fromRoot.getMyHighlights)
			.skipWhile(highlights => !highlights || highlights.length === 0)
			.subscribe(highlights => {
				let removeTheseHighlights = _.differenceBy(this.knownHighlights, highlights, 'wrestlerId');
				removeTheseHighlights.forEach(removeHighlight =>{
					let foundWrestler = this.allWrestlersByWeight[removeHighlight.wrestlerWeight].find(weightWrestler => weightWrestler.id == removeHighlight.wrestlerId);
					if (!!foundWrestler) {
						foundWrestler.highlight = null;
					}
				});
				highlights.forEach(highlight => {
					let weight = highlight.wrestlerWeight;
					let foundWrestler = this.allWrestlersByWeight[weight].find(weightWrestler => weightWrestler.id == highlight.wrestlerId);
					if (!!foundWrestler) {
						foundWrestler.highlight = highlight;
					}
				});
				this.knownHighlights = highlights;
			});
		this.liveDraftSubscription$ = this._store.select(fromRoot.getLiveDraftedWrestlers)
			.skipWhile(draftedWrestlers => !draftedWrestlers || draftedWrestlers.length <= 0)
			.subscribe(draftedWrestlers => {
				let addTheseWrestlers = _.differenceBy(draftedWrestlers, this.knownDrafts, 'wrestlerId');
				addTheseWrestlers.forEach(addWrestler => {
					if (!addWrestler) return;
					let weight = addWrestler.wrestlerWeight
					let foundWrestler = this.allWrestlersByWeight[weight].find(weightWrestler => weightWrestler.id == addWrestler.wrestlerId);
					if (!!foundWrestler) {
						foundWrestler.selected = addWrestler;
						let showNum = this.isShowing[weight] ? 100 : DEFAULT_SHOW_NUM;
						this.showWrestlersByWeight[weight] = [...this.allWrestlersByWeight[weight].slice(0, showNum)];
					}

					if (foundWrestler.selected.draftedById === this.myId){
						this.myTeam[foundWrestler.weight] = foundWrestler;
					}
				});
				this.knownDrafts = [...draftedWrestlers];
			});
	}

	private initShowWrestlersByWeight() {
		this.allWeights.forEach(weight => {
			this.showWrestlersByWeight[weight] = [...this.allWrestlersByWeight[weight].slice(0, DEFAULT_SHOW_NUM)];
		})
	}

	protected Click_ShowAll(weight: string): void {
		this.isShowing[weight] = true;
		this.showWrestlersByWeight[weight] = [...this.allWrestlersByWeight[weight]];
	}

	//TODO going to have to makes this part of the store
	protected IsShowingAll(weight: string): boolean {
		if (this.isShowing.hasOwnProperty(weight)) {
			return this.isShowing[weight];
		}
		return false;
	}
	
	Click_OpenWrestler(wrestler: IWrestler) {
		let wrestlerModal = this.modalCtrl.create(WrestlerModalPage, { wrestler: wrestler });
		wrestlerModal.present();
	}

	Click_OpenWeight(weight: string) {
		let wrestlerModal = this.modalCtrl.create(WeightModal, { weight: weight });
		wrestlerModal.present();
	}

	Click_ClickHideSelectedWrestlers() {
		this.isHidingSelected = !this.isHidingSelected;		
	}
	Click_ClickHideMySelectedWeights() {
		this.isHidingMyWeight = !this.isHidingMyWeight;	
	}
	Click_ClickAdminHideAll() {
		this.isAdminHide = !this.isAdminHide;
		if (this.isAdminHide){
			this.isHidingMyWeight = !this.isAdminHide;
		}
		this.isHidingSelected = this.isAdminHide;
	}

}