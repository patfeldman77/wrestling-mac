import { ResultsPage } from './../results.page/results.page';
import { MyTeamPage } from './../my-team.page/my-team.page';
import { IDraftUser } from './../../../store/models/user.interface';
import { LeaguePage } from './../league.page/league.page';
import { AccountPage } from './../../../league-pages/pages/account/account';
import { ILeague } from './../../../store/models/league.interface';
import { Observable } from 'rxjs/Observable';
import { AppState } from './../../../store/wrestling.store';
import { Store } from '@ngrx/store';
import { WrestlersPage } from './../wrestlers-page/wrestlers-page';
import { NavController, NavParams } from 'ionic-angular';
import { Component } from '@angular/core';
import * as fromRoot from '../../../store/wrestling.store';

@Component({
	templateUrl: 'tabs.html'
})
export class TabsPage {
	// this tells the tabs component which Pages
	// should be each tab's root Page
	tab1Root: any = LeaguePage;
	tab2Root: any = WrestlersPage;
	tab3Root: any = MyTeamPage;
	isAdmin: boolean = true;

	private $currentLeague: Observable<ILeague>;
	private $isDrafting : Observable<boolean>;
	private $isDraftComplete : Observable<boolean>;
	private $myId : Observable<string>;
	private $users : Observable<dictionary<IDraftUser>>;
	private $timeTillDraftInMS : Observable<number>;
	private $numberDrafted : Observable<number>;
	private $currentRound: Observable<number>;

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		public _store: Store<AppState>) {
		this.$currentLeague = this._store.select(fromRoot.getCurrentLeague).skipWhile(league => !league).take(1);
		this.$isDrafting = this._store.select(fromRoot.isDrafting);
		this.$isDraftComplete = this._store.select(fromRoot.isDraftComplete);
		this.$myId = this._store.select(fromRoot.getMyId);
		this.$users = this._store.select(fromRoot.getDraftUsers).skipWhile(users => !users || Object.keys(users).length == 0).take(1);
		this.$timeTillDraftInMS = this._store.select(fromRoot.getTimeTillDraftInMS);
		this.$numberDrafted = this._store.select(fromRoot.getNumberOfWrestlersDrafted);
		this.$currentRound = this._store.select(fromRoot.getCurrentRound);
		this._store.select(fromRoot.isCurrentLeagueAdmin).subscribe(isAdmin => { this.isAdmin = isAdmin; })
	}

	protected Click_Dashboard() {
		this.navCtrl.setRoot(AccountPage);
	}
}
