import { WeightModal } from './../../pages-modal/weight.modal/weight.modal';
import { IDraftedWrestler } from './../../../store/models/league.interface';
import { WrestlerModalPage } from './../../pages-modal/wrestler-modal/wrestler-modal';
import { IWrestlerHighlight, defaultHighlights } from './../../../info/highlights.class';
import { IWrestler } from './../../../store/models/wrestlers.interface';
import { IUserWithKey } from './../../../store/models/user.interface';
import { Observable } from 'rxjs/Observable';
import { AppState } from './../../../store/wrestling.store';
import { Store } from '@ngrx/store';
import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import * as _ from 'lodash';
import * as fromRoot from '../../../store/wrestling.store';

@Component({
	selector: 'page-my-team',
	templateUrl: 'my-team.page.html'
})
export class MyTeamPage {
	private $user: Observable<IUserWithKey>;
	private $wrestlers: Observable<dictionary<IWrestler>>;
	private $highlights: Observable<IWrestlerHighlight[]>;
	private $draftSlot: Observable<number>;
	private $draftedTeam: Observable<IDraftedWrestler[]>;
	private $isDraftComplete: Observable<boolean>;

	private allWrestlers: dictionary<IWrestler> = null;
	private myWrestlers: dictionary<IWrestler[]> = {};
	private knownDrafts: IDraftedWrestler[];


	protected highlightInfo = defaultHighlights;

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		public _store: Store<AppState>,
		public modalCtrl: ModalController) {
	}

	ionViewDidLoad() {
			this.highlightInfo.forEach(dHighlight => {
				this.myWrestlers[dHighlight.id] = [];
			});
			this.$user = this._store.select(fromRoot.getMyInfo);
			this.$isDraftComplete = this._store.select(fromRoot.isDraftComplete).take(1);
			this.$draftedTeam = this._store.select(fromRoot.getMyDraftedTeam);
			this.$draftSlot = this._store.select(fromRoot.getMyDraftSlot).take(1);
			this.$wrestlers = this._store.select(fromRoot.getAllWrestlersById).take(1);
			this.$highlights = this._store.select(fromRoot.getMyHighlights);
	
			this.$highlights.subscribe(highlights => {
				if (!!this.allWrestlers) {
					this.highlightInfo.forEach(dHighlight => {
						this.myWrestlers[dHighlight.id] = [];
					});
					highlights.forEach(highlight => {
						let newWrestler = this.allWrestlers[highlight.wrestlerId];
						newWrestler.highlight = highlight;
						this.myWrestlers[highlight.highlightId].push(newWrestler);
					})
					// reset the list to check them all again
					this.knownDrafts = [];
					// force the first check
					this._store.select(fromRoot.getLiveDraftedWrestlers).take(1).subscribe(draftedWrestlers => {
						this.checkMyHighlights(draftedWrestlers);
					});		
					
				}
			});
			Observable.zip(this.$user, this.$wrestlers, this.$highlights,
				(myInfo: IUserWithKey, allWrestlers: dictionary<IWrestler>, highlights: IWrestlerHighlight[]) => ({ myInfo, allWrestlers, highlights }))
				.subscribe(combo => {
					this.allWrestlers = _.cloneDeep(combo.allWrestlers);
					combo.highlights.forEach(highlight => {
						let newWrestler = this.allWrestlers[highlight.wrestlerId];
						newWrestler.highlight = highlight;
						this.myWrestlers[highlight.highlightId].push(newWrestler);
					})
				});
	
			this._store.select(fromRoot.getLiveDraftedWrestlers).subscribe(draftedWrestlers => {
				this.checkMyHighlights(draftedWrestlers);
			});		
	
	}

	protected checkMyHighlights(draftedWrestlers: IDraftedWrestler[]){
		if (!draftedWrestlers || draftedWrestlers.length) return;
		let somethingFound = false;
		let checkDrafts = _.differenceBy( draftedWrestlers, this.knownDrafts, 'wrestlerId');
		this.highlightInfo.forEach(dHighlight => {
			checkDrafts.forEach(draftedWrestler => {
				let foundIdx = this.myWrestlers[dHighlight.id].findIndex(wrestler =>{
					return wrestler.id == draftedWrestler.wrestlerId
				})
				if (foundIdx >= 0){
					this.myWrestlers[dHighlight.id].splice(foundIdx,1);
					somethingFound = true

				}
			})
		});
		if (somethingFound)
			this.knownDrafts = draftedWrestlers;

	}

	Click_OpenWeight(weight: string) {
		let wrestlerModal = this.modalCtrl.create(WeightModal, { weight: weight });
		wrestlerModal.present();
	}
	
	Click_OpenWrestler(wrestler: IWrestler) {
		let wrestlerModal = this.modalCtrl.create(WrestlerModalPage, { wrestler: wrestler });
		wrestlerModal.present();
	}
}
