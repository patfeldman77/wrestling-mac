import { ILeagueTeam } from './../../../store/models/league-team.interface';
import { WeightModal } from './../../pages-modal/weight.modal/weight.modal';
import { WrestlerModalPage } from './../../pages-modal/wrestler-modal/wrestler-modal';
import { IWrestler } from './../../../store/models/wrestlers.interface';
import { IDraftUser } from './../../../store/models/user.interface';
import { ILeague, IDraftedWrestler } from './../../../store/models/league.interface';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { AppState } from './../../../store/wrestling.store';
import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ModalController } from 'ionic-angular';

import * as _ from 'lodash';
import * as fromRoot from '../../../store/wrestling.store';
import * as fromDraft from '../../../store/draft.store/draft.reducer';

@Component({
	selector: 'page-league',
	templateUrl: 'league.page.html'
})
export class LeaguePage {
	private $currentLeague: Observable<ILeague>;
	private $draftData: Observable<fromDraft.State>;
	private $liveDraftedTeams: Observable<dictionary<IDraftedWrestler[]>>;
	private $draftUsers: Observable<dictionary<IDraftUser>>;
	private $isDraftComplete: Observable<boolean>;
	private teams: ILeagueTeam[] = [];

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		public alertCtrl: AlertController,
		public modalCtrl: ModalController,
		public _store: Store<AppState>) {
		this.$isDraftComplete = this._store.select(fromRoot.isDraftComplete).take(2);
		this.$currentLeague = this._store.select(fromRoot.getCurrentLeague).skipWhile(league => !league).take(1);
		this.$draftData = this._store.select(fromRoot.getDraft).take(1);
		this.$liveDraftedTeams = this._store.select(fromRoot.getLiveDraftedTeams);
		this.$draftUsers = this._store.select(fromRoot.getDraftUsers);

		// USED TO PRINT THE TEAMS IN CSV FORM
		// this.$liveDraftedTeams.subscribe(teams =>{
		// 	this._store.select(fromRoot.getAllWrestlersById).take(1).subscribe(all =>{
		// 		if (!!teams ){
		// 			let teamarr = _.map(teams, (value, key) => value);
		// 			teamarr.forEach(team =>{
		// 				//console.log(team);
		// 				team.sort((a,b) => parseInt(a.wrestlerWeight) - parseInt(b.wrestlerWeight)).forEach(wrestler =>{
		// 					let chosen = all[wrestler.wrestlerId];
		// 					let owner = wrestler.draftedByName;
		// 					let weight = chosen.weight;
		// 					let name = chosen.first_name + ' ' + chosen.last_name;
		// 					console.log(owner + "," + weight + ',' + chosen.rank + ',' + name);
		// 				});
		// 			});
		// 		}
		// 	})
		// });

		this.$isDraftComplete.subscribe(isComplete => {
			if (isComplete) {
				let $all = this._store.select(fromRoot.getAllWrestlersById).take(1);
				let $users = this.$draftUsers.skipWhile(users => Object.keys(users).length === 0).take(1);
				let $teams = this.$liveDraftedTeams.skipWhile(teams => Object.keys(teams).length === 0).take(1);
				Observable.zip($all, $users, $teams,
					(all: dictionary<IWrestler>, users: dictionary<IDraftUser>, teams: dictionary<IDraftedWrestler[]>) => ({ all, users, teams }))
					.subscribe(zip => {
						let userArr:IDraftUser[] = _.map(zip.users, (value, key) => value);
						Object.keys(zip.teams).forEach(teamKey => {
							let user = userArr.find(user=>user.key === teamKey);
							let newTeam: ILeagueTeam = {
								user: user,
								team: [],
								teamScore: 0
							}
							zip.teams[teamKey].forEach(draftedWrestler => {
								//let addWrestler = _.cloneDeep(zip.all[draftedWrestler.wrestlerId]);
								let addWrestler = zip.all[draftedWrestler.wrestlerId];
								newTeam.team.push(addWrestler);
								newTeam.teamScore += addWrestler.matches.reduce((acc, match) => acc + match.teamPoints, 0);
							});
							this.teams.push(newTeam);
						})
						this.teams = this.teams.sort((a,b) => b.teamScore - a.teamScore);
						this.teams.forEach(team =>{
							console.log (team.user.name + ':' + team.teamScore);
						})
					});
			}
		})
	}

	public Expectations() {
		let alert = this.alertCtrl.create({
			title: 'What do you want from me!',
			message: 'I ain\'t got no time for no jibba jabba!',
			buttons: [{ text: 'COME ON!', role: 'cancel' }]
		});
		alert.present();

	}
	Click_OpenWrestler(wrestler: IWrestler) {
		let wrestlerModal = this.modalCtrl.create(WrestlerModalPage, { wrestler: wrestler });
		wrestlerModal.present();
	}

	Click_OpenWeight(weight: string) {
		let wrestlerModal = this.modalCtrl.create(WeightModal, { weight: weight });
		wrestlerModal.present();
	}

}
