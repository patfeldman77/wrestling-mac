import { IDraftedWrestler } from './../../../store/models/league.interface';
import { IRulesResult, RuleType, Rules } from './../../../util/rules.class';
import { WrestlerModalPage } from './../wrestler-modal/wrestler-modal';
import { Observable } from 'rxjs/Rx';
import { AppState } from './../../../store/wrestling.store';
import { Store } from '@ngrx/store';
import { IWrestler } from './../../../store/models/wrestlers.interface';
import { defaultHighlights } from './../../../info/highlights.class';
import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, ModalController, AlertController } from 'ionic-angular';
import * as _ from 'lodash';
import * as fromRoot from '../../../store/wrestling.store';
import * as draftActions from '../../../store/draft.store/draft.actions';
import * as wrestlersActions from '../../../store/wrestlers.store/wrestlers.actions';

@Component({
	selector: 'weight-modal',
	templateUrl: 'weight.modal.html'
})
export class WeightModal {
	protected highlights = defaultHighlights;
	protected wrestlers: IWrestler[];
	protected weight: string;
	private $isMyTurn: Observable<boolean>;
	private myTeam: IDraftedWrestler[];

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		private viewCtrl: ViewController,
		private modalCtrl: ModalController,
		private alertCtrl: AlertController,
		private _store: Store<AppState>) {
		this.weight = navParams.get('weight');
	}

	ionViewDidLoad() {
		this.$isMyTurn = this._store.select(fromRoot.isMyTurn).take(1);
		this._store.dispatch(new wrestlersActions.SetWeightAction(this.weight));
		this._store.select(fromRoot.getWrestlersAtSelectedWeight).take(1).subscribe(wrestlers => {
			this.wrestlers = _.cloneDeep(wrestlers);
		});
		this._store.select(fromRoot.getMyDraftedTeam).take(1).subscribe(team =>{
			this.myTeam = team;			
		})

		this._store.select(fromRoot.getMyHighlights).take(1).subscribe(highlights => {
			highlights.filter(highlight => highlight.wrestlerWeight == this.weight).forEach(highlight => {
				let newWrestler = this.wrestlers.find(wrestler => wrestler.id == highlight.wrestlerId);
				if (!!newWrestler) {
					newWrestler.highlight = highlight;
				}
			});
		});
		this._store.select(fromRoot.getLiveDraftedWrestlers)
			.skipWhile(draftedWrestlers => !draftedWrestlers || draftedWrestlers.length <= 0)
			.take(1)
			.subscribe(draftedWrestlers => {
				draftedWrestlers.filter(draftedWrestler => draftedWrestler.wrestlerWeight == this.weight).forEach(draftedWrestler => {
					let newWrestler = this.wrestlers.find(wrestler => wrestler.id.toString() == draftedWrestler.wrestlerId);
					if (!!newWrestler) {
						newWrestler.selected = draftedWrestler;
					}
				});
			});

	}

	Click_Close() {
		this.viewCtrl.dismiss("Closed");
	}
	Click_OpenWrestler(wrestler: IWrestler) {
		this.viewCtrl.dismiss("closed");
		let wrestlerModal = this.modalCtrl.create(WrestlerModalPage, { wrestler: wrestler });
		wrestlerModal.present();
	}

	protected CheckRules(wrestler): IRulesResult{
		if (!this.myTeam) return { passes: false, reasons: ['NO TEAM']};
		let retVal = Rules.CheckRule(RuleType.EleventhPick, { selection: wrestler, team: this.myTeam});
		return retVal;
	}

	protected ShowRulesAlert(rules: IRulesResult){
		let alert = this.alertCtrl.create({
			title: 'CANNOT SELECT!',
			message: 'Sorry this selection would violate the following rules: 1. ' + rules.reasons[0],
			buttons: [
				{
					text: 'Cancel',
					role: 'cancel',
				},
			]
		});
		alert.present();		
	}

	protected Click_AddWrestlerToTeam(wrestler: IWrestler){
		let rules = this.CheckRules(wrestler);
		if (!rules.passes){
			this.ShowRulesAlert(rules)
		} else {
			let alert = this.alertCtrl.create({
				title: 'Add Wrestler',
				message: 'Are you sure you want to add ' + wrestler.first_name + ' ' + wrestler.last_name + ' to the your team?',
				buttons: [
					{
						text: 'Cancel',
						role: 'cancel',
					},
					{
						text: 'Add',
						handler: () => {
							this.viewCtrl.dismiss({
								wrestler: wrestler
							});
							this._store.dispatch(new draftActions.SelectWrestlerAction(wrestler));
						}
					}
				]
			});
			alert.present();
		}
	}

	// Click_Highlight(highlight: IHighlightDetails){
	// 	this.viewCtrl.dismiss("Closed");		
	// 	this._store.dispatch(new userActions.AddWrestlerHighlightAction({wrestler: this.wrestler, highlight: highlight}));
	// }
	// Click_RemoveHighlight(){
	// 	this.viewCtrl.dismiss("Closed");		
	// 	this._store.dispatch(new userActions.RemoveWrestlerHighlightAction(this.wrestler));
	// }
}
