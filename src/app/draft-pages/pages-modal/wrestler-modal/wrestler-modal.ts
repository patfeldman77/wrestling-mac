import { Rules, RuleType, IRulesResult } from './../../../util/rules.class';
import { IDraftedWrestler } from './../../../store/models/league.interface';
import { IDraftUser } from './../../../store/models/user.interface';
import { HighlightModalPage } from './../highlight.modal/highlight.modal';
import { Observable } from 'rxjs/Observable';
import { IWrestler } from './../../../store/models/wrestlers.interface';
import { AppState } from './../../../store/wrestling.store';
import { Store } from '@ngrx/store';
import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, ModalController, AlertController } from 'ionic-angular';
import * as _ from 'lodash';
import * as fromRoot from '../../../store/wrestling.store';
import * as draftActions from '../../../store/draft.store/draft.actions';
import * as wrestlersActions from '../../../store/wrestlers.store/wrestlers.actions';
//import * as userActions from '../../store/user.store/user.actions';
@Component({
	selector: 'wrestler-modal',
	templateUrl: 'wrestler-modal.html'
})
export class WrestlerModalPage {
	protected $isMyTurn: Observable<boolean>;
	protected $isAdmin: Observable<boolean>;
	protected $isDrafting: Observable<boolean>;
	protected $isDraftComplete: Observable<boolean>;
	private wrestler: IWrestler;
	private currentTeam: IDraftedWrestler[];
	private ruleCheck: IRulesResult = { passes: true, reasons: [] };
	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		public _store: Store<AppState>,
		public viewCtrl: ViewController,
		public modalCtrl: ModalController,
		public alertCtrl: AlertController) {
		this.wrestler = navParams.get('wrestler');
		this.$isDrafting = this._store.select(fromRoot.isDrafting).take(1);
		this.$isDraftComplete = this._store.select(fromRoot.isDraftComplete).take(1);
		this.$isMyTurn = this._store.select(fromRoot.isMyTurn).take(1);
		this.$isAdmin = this._store.select(fromRoot.isCurrentLeagueAdmin).take(1);
		this.$isAdmin.subscribe(isAdmin => {
			if (isAdmin) {
				this._store.select(fromRoot.getCurrentDraftUser)
					.take(1)
					.subscribe(user => {
						this._store.select(fromRoot.getCurrentDraftTeam).take(1).subscribe(currentTeam => {
							this.currentTeam = currentTeam;
							this.CheckRules(this.currentTeam);
						});
					});
			} else {
				this._store.select(fromRoot.getMyDraftedTeam).take(1).subscribe(currentTeam => {
					this.currentTeam = currentTeam;
					this.CheckRules(this.currentTeam);
				});

			}
		});
	}

	protected Click_Close() {
		this.viewCtrl.dismiss("Closed");
	}

	protected Click_OpenWrestler(name: string){
		if (name.indexOf(this.wrestler.last_name) < 0){
			this._store.dispatch(new wrestlersActions.SetWrestlerByNameAction({name: name, weight: this.wrestler.weight}));
			this._store.select(fromRoot.getSelectedWrestler).take(1).subscribe(wrestler =>{
				this.viewCtrl.dismiss("closed");
				let wrestlerModal = this.modalCtrl.create(WrestlerModalPage, { wrestler: wrestler });
				wrestlerModal.present();
			})
		}		
	}

	protected CheckRules(team: IDraftedWrestler[]) {
		this.ruleCheck = Rules.CheckRule(RuleType.EleventhPick, { selection: this.wrestler, team: team });
	}
	protected ShowRulesAlert() {
		let alert = this.alertCtrl.create({
			title: 'CANNOT SELECT!',
			message: 'Sorry this selection would violate the following rules: 1. ' + this.ruleCheck.reasons[0],
			buttons: [
				{
					text: 'Cancel',
					role: 'cancel',
				},
			]
		});
		alert.present();
	}
	protected Click_AddWrestlerToTeamByAdmin() {
		this.CheckRules(this.currentTeam);
		if (!this.ruleCheck.passes) {
			this.ShowRulesAlert()
		} else {
			let alert = this.alertCtrl.create({
				title: 'ADMIN WARNING: NOT FOR YOUR TEAM!',
				message: 'Are you sure you want to add ' + this.wrestler.first_name + ' ' + this.wrestler.last_name + ' to the CURRENT team?',
				buttons: [
					{
						text: 'Cancel',
						role: 'cancel',
					},
					{
						text: 'Admin Add',
						handler: () => {
							this.viewCtrl.dismiss({
								wrestler: this.wrestler
							});
							this._store.dispatch(new draftActions.SelectWrestlerToCurrentTeamAction(this.wrestler));
						}
					}
				]
			});
			alert.present();
		}
	}
	protected Click_AddWrestlerToTeam() {
		this.CheckRules(this.currentTeam);
		if (!this.ruleCheck.passes) {
			this.ShowRulesAlert()
		} else {

			let alert = this.alertCtrl.create({
				title: 'Add Wrestler',
				message: 'Are you sure you want to add ' + this.wrestler.first_name + ' ' + this.wrestler.last_name + ' to the your team?',
				buttons: [
					{
						text: 'Cancel',
						role: 'cancel',
					},
					{
						text: 'Add',
						handler: () => {
							this.viewCtrl.dismiss({
								wrestler: this.wrestler
							});
							this._store.dispatch(new draftActions.SelectWrestlerAction(this.wrestler));
						}
					}
				]
			});
			alert.present();
		}
	}

	protected Click_AddHighlightToWrestler() {
		this.viewCtrl.dismiss("closed");
		let highlightModal = this.modalCtrl.create(HighlightModalPage, { wrestler: this.wrestler });
		highlightModal.present();
	}

}
