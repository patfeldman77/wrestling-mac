import { AppState } from './../../../store/wrestling.store';
import { Store } from '@ngrx/store';
import { IWrestler } from './../../../store/models/wrestlers.interface';
import { defaultHighlights, IHighlightDetails } from './../../../info/highlights.class';
import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import * as userActions from '../../../store/user.store/user.actions';

@Component({
	selector: 'highlight-modal',
	templateUrl: 'highlight.modal.html'
})
export class HighlightModalPage {
	protected highlights = defaultHighlights;
	protected wrestler: IWrestler;
	constructor(
		public navCtrl: NavController, 
		public navParams: NavParams, 
		private viewCtrl: ViewController, 
		private _store: Store<AppState>) {
		this.wrestler = navParams.get('wrestler');
	}

	Click_Close()  {
		this.viewCtrl.dismiss("Closed");		
	}
	Click_Highlight(highlight: IHighlightDetails){
		this.viewCtrl.dismiss("Closed");		
		this._store.dispatch(new userActions.AddWrestlerHighlightAction({wrestler: this.wrestler, highlight: highlight}));
	}
	Click_RemoveHighlight(){
		this.viewCtrl.dismiss("Closed");		
		this._store.dispatch(new userActions.RemoveWrestlerHighlightAction(this.wrestler));
	}
}
