import { IDraftUser } from './../../../store/models/user.interface';
import { weights } from './../../../info/weights';
import { ISubscription } from 'rxjs/Subscription';
import { IDraftedWrestler, ILeague } from './../../../store/models/league.interface';
import { IWrestler } from './../../../store/models/wrestlers.interface';
import { AppState } from './../../../store/wrestling.store';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import * as _ from 'lodash';
import * as fromRoot from '../../../store/wrestling.store';

const DEFAULT_SHOW_NUM = 16;
@Component({
	selector: 'follow-draft',
	templateUrl: 'follow-draft.modal.html'
})
export class FollowDraftModal {
	private allWeights: string[];
	private allWrestlersByWeight: dictionary<IWrestler[]> = {};
	private showWrestlersByWeight: dictionary<IWrestler[]> = {};
	private knownDrafts: IDraftedWrestler[] = [];
	private liveDraftSubscription$: ISubscription;
	private currentLeagueSubscription$: ISubscription;
	private isShowing: dictionary<boolean> = {}


	// for DraftHeaderComponent	
	private currentLeague: ILeague;
	private $isDrafting : Observable<boolean>;
	private $users : Observable<dictionary<IDraftUser>>;
	private $timeTillDraftInMS : Observable<number>;
	private $numberDrafted : Observable<number>;
	private $currentRound: Observable<number>;

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		public modalCtrl: ModalController,
		public _store: Store<AppState>) { }

	ionViewDidLoad() {
		this.$isDrafting = this._store.select(fromRoot.isDrafting);
		this.$users = this._store.select(fromRoot.getDraftUsers).skipWhile(users => !users || Object.keys(users).length == 0).take(1);
		this.$timeTillDraftInMS = this._store.select(fromRoot.getTimeTillDraftInMS);
		this.$numberDrafted = this._store.select(fromRoot.getNumberOfWrestlersDrafted);
		this.$currentRound = this._store.select(fromRoot.getCurrentRound);
		
		this._store.select(fromRoot.getWrestlers).skipWhile(wrestlersState => !wrestlersState.wrestlersLoaded).take(1).subscribe(wrestlersState => {
			this.allWeights = weights;
			let allWrestlers = _.cloneDeep(wrestlersState.wrestlers.wrestlersById);
			this.allWeights.forEach(weight => {
				this.allWrestlersByWeight[weight] = _.map(allWrestlers, (value, key) => value)
					.filter(wrestler => {
						return !!wrestler && wrestler.weight == weight;
					}).sort((a: IWrestler, b: IWrestler) => a.rank - b.rank);
			});
			this.initShowWrestlersByWeight();
		});
		this.currentLeagueSubscription$ = this._store.select(fromRoot.getCurrentLeague).subscribe(league => {
			if (!league && !!this.liveDraftSubscription$) {
				this.liveDraftSubscription$.unsubscribe();
				this.currentLeagueSubscription$.unsubscribe();
			} else {
				this.currentLeague = league;
			}
		});
		this.liveDraftSubscription$ = this._store.select(fromRoot.getLiveDraftedWrestlers)
			.skipWhile(draftedWrestlers => !draftedWrestlers || draftedWrestlers.length <= 0)
			.subscribe(draftedWrestlers => {
				let addTheseWrestlers = _.differenceBy(draftedWrestlers, this.knownDrafts, 'wrestlerId');
				addTheseWrestlers.forEach(addWrestler => {
					let weight = addWrestler.wrestlerWeight
					let foundWrestler = this.allWrestlersByWeight[weight].find(weightWrestler => weightWrestler.id == addWrestler.wrestlerId);
					if (!!foundWrestler) {
						foundWrestler.selected = addWrestler;
						let showNum = this.isShowing[weight] ? 100 : DEFAULT_SHOW_NUM;
						this.showWrestlersByWeight[weight] = [...this.allWrestlersByWeight[weight].slice(0, showNum)];
					}
				});
				this.knownDrafts = [...draftedWrestlers];
			});
	}

	private initShowWrestlersByWeight() {
		this.allWeights.forEach(weight => {
			this.showWrestlersByWeight[weight] = [...this.allWrestlersByWeight[weight].slice(0, DEFAULT_SHOW_NUM)];
		})
	}

	protected Click_ShowAll(weight: string): void {
		this.isShowing[weight] = true;
		this.showWrestlersByWeight[weight] = [...this.allWrestlersByWeight[weight]];
	}

	//TODO going to have to makes this part of the store
	protected IsShowingAll(weight: string): boolean {
		if (this.isShowing.hasOwnProperty(weight)) {
			return this.isShowing[weight];
		}
		return false;
	}
}