import { IWrestlers} from './../models/wrestlers.interface';
import { createSelector } from 'reselect';
import * as actions from './wrestlers.actions';
import * as _ from 'lodash';

export interface State {
	wrestlers: IWrestlers;
	wrestlersLoaded: boolean;
	selectedWeight: number;
	selectedWrestlerId: number;
} ;

const initialState: State = {
	wrestlers: {
		wrestlersByWeight: {},
		wrestlersById: {}
	}, 
	wrestlersLoaded: false,
	selectedWeight: null, 
	selectedWrestlerId: null

};
export function reducer(state = initialState, action: actions.Actions): State {
	switch (action.type) {
		case actions.ActionTypes.LOAD: {
			return state;
		}
		case actions.ActionTypes.LOAD_COMPLETE: {
			return Object.assign({}, state, {
				wrestlers: action.payload,
				wrestlersLoaded: true
			});
		}
		case actions.ActionTypes.SET_WEIGHT: {
			return Object.assign({}, state, {
				selectedWeight: action.payload,
			});			
		}
		case actions.ActionTypes.SET_WRESTLER_BY_NAME: {
			let name = action.payload.name;
			let weight = action.payload.weight;
			let nameArr = name.split(' ');
			let firstName = nameArr[0];
			let lastName = nameArr[1];
			let found = 0;
			let setIndex = -1;
			let foundArr = [];
			state.wrestlers.wrestlersByWeight[weight].forEach(index =>{
				let wrestler = state.wrestlers.wrestlersById[index];
				if (wrestler.last_name.toLowerCase().indexOf(lastName.toLowerCase()) >= 0) {
					found++ 
					foundArr.push(index);
					setIndex = index;
				}
			})
			
			if (found > 1){
				found = 0;
				foundArr.forEach(index =>{
					let wrestler = state.wrestlers.wrestlersById[index];
					if (wrestler.first_name.toLowerCase().indexOf(firstName.toLowerCase()) >= 0) {
						setIndex = index;
						found++
					}					
				});
				if (found===0) {
					console.log ('ERROR: Name Conflict with ' + name);
				}
			}

			if (found > 0){
				return Object.assign({}, state, {
					selectedWrestlerId: setIndex
				});	
			}
			return state;

		}
		default: {
			return state;
		}
	}	
};

export const getWrestlers = (state: State) => state.wrestlers;
export const getSelectedWrestlerId = (state: State) => state.selectedWrestlerId;
export const getSelectedWeight = (state: State) => state.selectedWeight;
export const getWrestlersById = (state: State) => state.wrestlers.wrestlersById;
export const getAllWeights = createSelector(getWrestlers, (wrestlers) => {
	return Object.keys(wrestlers.wrestlersByWeight);
} );
export const getSelectedWrestler = createSelector(getWrestlers, getSelectedWrestlerId, (wrestlers, id) =>{
	return wrestlers.wrestlersById[id];
});

export const getAllWrestlersByWeight = createSelector(getWrestlers,  (wrestlers) =>{
	let retVal:any = {}; 
	if (Object.keys(wrestlers.wrestlersByWeight).length === 0 ) return null;
	_.forIn(wrestlers.wrestlersByWeight, (value, key) => {
		retVal[key] = (wrestlers.wrestlersByWeight[key].map(wrestlerId => wrestlers.wrestlersById[wrestlerId]));		
	})
	return retVal;
});

export const getWrestlersAtSelectedWeight = createSelector(getWrestlers, getSelectedWeight, (wrestlers, weight) =>{
	return wrestlers.wrestlersByWeight[weight].map(wrestlerId => wrestlers.wrestlersById[wrestlerId]);
});
