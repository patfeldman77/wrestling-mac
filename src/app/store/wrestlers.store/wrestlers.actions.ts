import { IDraftedWrestler } from './../models/league.interface';
import { IWrestlers } from './../models/wrestlers.interface';
import { Action } from '@ngrx/store';
import { type } from '../util';
/**
 * For each action type in an action group, make a simple
 * enum object for all of this group's action types.
 * 
 * The 'type' utility function coerces strings into string
 * literal types and runs a simple check to guarantee all
 * action types in the application are unique. 
 */
export const ActionTypes = {
	LOAD: type('[WRESTLERS] Loading'),
	LOAD_COMPLETE: type('[WRESTLERS] Loading Complete'),
	SET_WEIGHT: type('[WRESTLERS] Set Weight'),
	MARK_ALL_SELECTED: type('[WRESTLERS] Mark ALL wrestlers Selected'),
	CLEAR_ALL_SELECTIONS: type('[WRESTLERS] Clear all Selections'),
	SET_WRESTLER_BY_NAME: type('[WRESTLERS] Set the current wrestler by name')
};

/**
 * Export a type alias of all actions in this action group
 * so that reducers can easily compose action types
 */
export type Actions = LoadCompleteAction 
	| LoadAction
	| SetWeightAction
	| MarkAllSelectedAction
	| SetWrestlerByNameAction
	| ClearAllSelectionsAction;

/**
 * Every action is comprised of at least a type and an optional
 * payload. Expressing actions as classes enables powerful 
 * type checking in reducer functions.
 * 
 * See Discriminated Unions: https://www.typescriptlang.org/docs/handbook/advanced-types.html#discriminated-unions
 */
export class LoadAction implements Action {
	type = ActionTypes.LOAD;
	constructor() { }
}

export class LoadCompleteAction implements Action {
	type = ActionTypes.LOAD_COMPLETE;
	constructor(public payload: IWrestlers) { }
}

export class SetWeightAction implements Action {
	type = ActionTypes.SET_WEIGHT;
	constructor(public payload: string) { }
}

export class SetWrestlerByNameAction implements Action {
	type = ActionTypes.SET_WRESTLER_BY_NAME;
	constructor(public payload: {weight: string, name: string}) { }
}

export class MarkAllSelectedAction implements Action {
	type = ActionTypes.MARK_ALL_SELECTED;
	constructor(public payload: dictionary<IDraftedWrestler>) { }
}
export class ClearAllSelectionsAction implements Action {
	type = ActionTypes.CLEAR_ALL_SELECTIONS;
	constructor(public payload: IWrestlers) { }
}

