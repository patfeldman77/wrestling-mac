import { ILeague } from './../models/league.interface';
import { IUserWithKey, IDraftUser } from './../models/user.interface';
import { WrestlingService } from './../providers/wrestling.api';
import { SortByDraftOrderPipe } from './../../pipes/sort-by-draft-order.pipe';
import { Observable } from 'rxjs/Rx';
import { ISubscription } from 'rxjs/Subscription';
import { AppState } from './../wrestling.store';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import * as _ from 'lodash';
import * as timeConvert from '../../util/time-convert';
import * as draftActions from './draft.actions';
import * as fromRoot from './../wrestling.store';
import * as wrestlerActions from './../wrestlers.store/wrestlers.actions';

@Injectable()
export class DraftEffects {
	private $numSelected_subscription: ISubscription;
	private $draftedByTeam_subscription: ISubscription;
	private $draftedParticipants_subscription: ISubscription;
	private $timer: ISubscription;
	constructor(
		private draftApi: WrestlingService,
		private actions$: Actions,
		private _store: Store<AppState>) { }

	@Effect({ dispatch: false })
	updateDraftOrder$ = this.actions$
		.ofType(draftActions.ActionTypes.UPDATE_DRAFT_ORDER)
		.map(query => {
			this._store.select(fromRoot.getCurrentLeague).take(1).subscribe(league => {
				this.draftApi.UpdateDraftOrder(league, query.payload).subscribe(done => {
				});
			})
		});

	@Effect({ dispatch: false })
	updateDraftTime$ = this.actions$
		.ofType(draftActions.ActionTypes.UPDATE_DRAFT_DATE)
		.map(query => {
			this._store.select(fromRoot.getCurrentLeague).take(1).subscribe(league => {
				this.draftApi.UpdateDraftDate(league, query.payload).subscribe(done => {
				});
			})
		});

	@Effect({ dispatch: false })
	addUser$ = this.actions$
		.ofType(draftActions.ActionTypes.ADD_PARTICIPANT_TO_DRAFT)
		.map(query => {
			this._store.select(fromRoot.getCurrentLeague).take(1).subscribe(league => {
				this.draftApi.AddParticipantToDraft(league, query.payload).subscribe(user => {
					if (!!user) {
						this._store.dispatch(new draftActions.AddParticipantToDraftCompleteAction(user));
					}
				});
			})
		});

	@Effect({ dispatch: false })
	deleteUser$ = this.actions$
		.ofType(draftActions.ActionTypes.DELETE_PARTICIPANT_FROM_DRAFT)
		.map(query => {
			this._store.select(fromRoot.getCurrentLeague).take(1).subscribe(league => {
				this.draftApi.DeleteParticipantFromDraft(league, query.payload).subscribe(user => {
					if (!!user) {
						this._store.dispatch(new draftActions.DeleteParticipantFromDraftCompleteAction(user));
					}
				});
			})
		});

	@Effect({ dispatch: false })
	checkDate$ = this.actions$
		.ofType(draftActions.ActionTypes.CHECK_IF_DRAFTING)
		.map(() => {
			this._store.select(fromRoot.getDraft)
				.skipWhile(draft => !draft.draftSetup.draftDate)
				.take(1)
				.subscribe(draft => {
					let timeCheckInMS = (draft.draftSetup.draftDate - Date.now());
					let timeCheckInMinutes = Math.floor(timeCheckInMS * timeConvert.MS_TO_MINUTES);
					let timeCheckInHours = Math.floor(timeCheckInMS * timeConvert.MS_TO_HOURS);
					let timerVal = 0; // one hour
					if (timeCheckInMS > 0 && timeCheckInHours < 3) {
						if (timeCheckInHours > 1) {
							timerVal = 1000 * 60 * 30; // check every 30 min
						} else if (timeCheckInMinutes > 30) {
							timerVal = 1000 * 60 * 10; // check every 10 min
						} else if (timeCheckInMinutes > 10) {
							timerVal = 1000 * 10; // check every 2 min
						} else if (timeCheckInMinutes > 3) {
							timerVal = 1000 * 40; // check every 40 seconds
						} else if (timeCheckInMinutes > 1) {
							timerVal = 1000 * 20; // check every 20 seconds
						} else {
							timerVal = 1000 * 1; // check every 1 seconds
						}
						if (this.$timer) this.$timer.unsubscribe();
						this.$timer = Observable.timer(timerVal).subscribe(time => {
							this._store.dispatch(new draftActions.CheckIfDraftingAction());
						});
					}
				});
		});

	@Effect({ dispatch: false })
	persistDraft$ = this.actions$
		.ofType(draftActions.ActionTypes.SUBSCRIBE_TO_DRAFT)
		.map(query => {
			this.draftApi.GetDraftSetup(query.payload.leagueKey, query.payload.isPrivate).take(1).subscribe(draftSetup => {
				this._store.dispatch(new draftActions.UpdateDraftSetupAction(draftSetup));
				this._store.dispatch(new draftActions.CheckIfDraftingAction());
				this._store.select(fromRoot.isDrafting).skipWhile(isDrafting => !isDrafting).take(1).subscribe(isDrafting => {
					let draftSortPipe = new SortByDraftOrderPipe();
					let draftUserValues = _.map(draftSetup.users, (value, key) => value);;
					this.unsubscribe();
					this._store.select(fromRoot.getMyInfo).take(1).subscribe(myInfo => {
						this.$numSelected_subscription = this.draftApi
							.GetLiveDraftNumSelectedConnection(query.payload.leagueKey, query.payload.isPrivate)
							.subscribe(numSelected => {
								let isItMe = draftSortPipe.isItMe(draftUserValues, numSelected.$value, myInfo.id);
								this._store.dispatch(new draftActions.ProcessIsMyTurnAction(isItMe));
								this._store.dispatch(new draftActions.UpdateNumberOfWrestlersDraftedAction(numSelected.$value));
							});
						this.$draftedParticipants_subscription = this.draftApi
							.GetLiveDraftParticipantsConnection(query.payload.leagueKey, query.payload.isPrivate)
							.subscribe(draftedParticipants => {
								this._store.dispatch(new wrestlerActions.MarkAllSelectedAction(draftedParticipants));
								this._store.dispatch(new draftActions.ProcessLiveDraftParticipantsUpdateAction(draftedParticipants));
							});
						this.$draftedByTeam_subscription = this.draftApi
							.GetLiveDraftTeamsConnection(query.payload.leagueKey, query.payload.isPrivate)
							.subscribe(draftedTeams => {
								this._store.dispatch(new draftActions.ProcessLiveDraftTeamUpdateAction(draftedTeams));
							});
					});
				})
			});
		});

	@Effect({ dispatch: false })
	unsubscribe$ = this.actions$
		.ofType(draftActions.ActionTypes.UNSUBSCRIBE_TO_DRAFT)
		.map(() => {
			this.unsubscribe();
			if (this.$timer) this.$timer.unsubscribe();
		});

	@Effect({ dispatch: false })
	selectWrestler$ = this.actions$
		.ofType(draftActions.ActionTypes.SELECT_WRESTLER)
		.map(query => {
			let myInfo$ = this._store.select(fromRoot.getMyInfo).take(1);
			let myDraftOrder$ = this._store.select(fromRoot.getMyDraftSlot).take(1);
			let currentRound$ = this._store.select(fromRoot.getCurrentRound).take(1);
			let currentLeague$ = this._store.select(fromRoot.getCurrentLeague).take(1);
			Observable.zip(myInfo$, myDraftOrder$, currentLeague$, currentRound$,
				(myInfo: IUserWithKey, mySlot: number, league: ILeague, round: number) => ({ myInfo, mySlot, league, round }))
				.subscribe(combo => {
					this.draftApi.SelectWrestler(query.payload, combo.myInfo, combo.league.$key, combo.league.isPrivate, combo.round, combo.mySlot);
				});
		});


	@Effect({ dispatch: false })
	selectWrestlerAdmin$ = this.actions$
		.ofType(draftActions.ActionTypes.SELECT_WRESTLER_TO_CURRENT_TEAM)
		.map(query => {
			let currentLeague$ = this._store.select(fromRoot.getCurrentLeague).take(1);
			let currentRound$ = this._store.select(fromRoot.getCurrentRound).take(1);
			let draftUsers$ = this._store.select(fromRoot.getDraftUsers).take(1);
			let numSelected$ = this._store.select(fromRoot.getNumberOfWrestlersDrafted).take(1);
			Observable.zip(draftUsers$, numSelected$, currentLeague$, currentRound$,
				(users: IDraftUser[], numSelected: number, league: ILeague, round: number) => ({ users, numSelected, league, round }))
				.subscribe(combo => {
					let draftUserValues = _.map(combo.users, (value, key) => value);
					let draftSortPipe = new SortByDraftOrderPipe();
					let currentUser = draftSortPipe.getCurrentUser(draftUserValues, combo.numSelected);
					let currentIUser: IUserWithKey = {
						name: currentUser.name,
						$key: currentUser.key,
						id: currentUser.id,
						email: '',
						photo: '',
						leagues_ref: {}, 
						highlights: []
					}
					this.draftApi.SelectWrestler(query.payload, currentIUser, combo.league.$key, combo.league.isPrivate, combo.round, currentUser.draftOrder);
				});
		});


	// helper function name(params:type) {

	private unsubscribe() {
		if (this.$numSelected_subscription) this.$numSelected_subscription.unsubscribe();
		if (this.$draftedByTeam_subscription) this.$draftedByTeam_subscription.unsubscribe();
		if (this.$draftedParticipants_subscription) this.$draftedParticipants_subscription.unsubscribe();
	}

}
