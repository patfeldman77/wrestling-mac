import { IDraftUser } from './../models/user.interface';
import { IDraftSetup, IDraftedWrestler } from './../models/league.interface';
import * as actions from './draft.actions';
import * as _ from 'lodash';
import * as util from '../../util/random-key';

export interface State {
	draftSetup: IDraftSetup;
	liveDraftedParticipants: IDraftedWrestler[];
	liveDraftedTeams: dictionary<IDraftedWrestler[]>;
	liveDraftNumSelected: number;
	liveDraftRound: number;
	isDrafting: boolean;
	isMyTurn: boolean;
	timeTillDraftInMS: number;
};

const initialState: State = {
	draftSetup: {
		users: {},
		draftDate: null,
		draftComplete: false
	},
	liveDraftedParticipants: [],
	liveDraftNumSelected: 0,
	liveDraftRound: 0,
	liveDraftedTeams: {},
	timeTillDraftInMS: null,
	isDrafting: false,
	isMyTurn: false,
};

export function reducer(state = initialState, action: actions.Actions): State {
	switch (action.type) {
		case (actions.ActionTypes.PROCESS_IS_MY_TURN): {
			return Object.assign({}, state, {
				isMyTurn: action.payload
			})
		}
		case (actions.ActionTypes.UPDATE_NUMBER_DRAFTED): {
			let numSelected = action.payload;
			let numUsers = Object.keys(state.draftSetup.users).length
			let round = Math.floor(numSelected / numUsers) + 1;
			return Object.assign({}, state, {
				liveDraftNumSelected: action.payload,
				liveDraftRound: round
			})
		}
		case (actions.ActionTypes.UNSUBSCRIBE_TO_DRAFT): {
			return initialState;
		}
		case (actions.ActionTypes.DELETE_PARTICIPANT_FROM_DRAFT_COMPLETE): {
			let user: IDraftUser = action.payload;
			let lessUsers = Object.assign({}, state.draftSetup.users);
			Object.keys(lessUsers).forEach((key) => {
				if (lessUsers[key].name === user.name) {
					delete lessUsers[key];
				}
			});

			let newDraftSetup = Object.assign({}, state.draftSetup, {
				users: lessUsers
			});
			return Object.assign({}, state, {
				draftSetup: newDraftSetup
			})

		}
		case (actions.ActionTypes.ADD_PARTICIPANT_TO_DRAFT_COMPLETE): {
			let user: IDraftUser = {
				name: action.payload.name,
				draftOrder: action.payload.draftOrder,
				key: util.randomKey(),
				id: '',
				email: '',
				photo: '',
				leagues_ref: null,
				highlights: []
			};
			let extraUsers = Object.assign({}, state.draftSetup.users);
			extraUsers[user.key] = user;
			let newDraftSetup = Object.assign({}, state.draftSetup, {
				users: extraUsers
			});
			return Object.assign({}, state, {
				draftSetup: newDraftSetup
			})

		}

		case (actions.ActionTypes.CHECK_IF_DRAFTING): {
			if (!state.draftSetup.draftDate) return state;
			let timeLeftInMS = state.draftSetup.draftDate - Date.now();
			let isDrafting = timeLeftInMS <= 0;
			return Object.assign({}, state, {
				isDrafting: isDrafting,
				timeTillDraftInMS: timeLeftInMS
			})
		}
		case (actions.ActionTypes.UPDATE_DRAFT_SETUP): {
			let load = action.payload as IDraftSetup;
			_.forEach(load.users, (user, key) => {
				if (!user.hasOwnProperty('key')) {
					user.key = key;
				}
			});

			return Object.assign({}, state, {
				draftSetup: action.payload
			})
		}

		case (actions.ActionTypes.PROCESS_LIVE_DRAFT_PARTICIPANTS_UPDATE): {
			let load = action.payload as dictionary<IDraftedWrestler>;
			let final: IDraftedWrestler[] = [];
			_.forEach(load, (wrestler, key) => {
				final.push(wrestler);
			});
			return Object.assign({}, state, {
				liveDraftedParticipants: final
			})
		}
		case (actions.ActionTypes.PROCESS_LIVE_DRAFT_TEAMS_UPDATE): {
			let load = action.payload as dictionary<dictionary<IDraftedWrestler>>;
			let final: dictionary<IDraftedWrestler[]> = {};
			_.forEach(load, (team, teamKey) => {
				final[teamKey] = []
				_.forEach(team, (wrestler, wrestlerKey) => {
					final[teamKey].push(wrestler);
				});
			});
			return Object.assign({}, state, {
				liveDraftedTeams: final
			})
		}
		default: {
			return state;
		}
	}
};

export const getDraftSetup = (state: State) => state.draftSetup;
export const getDraftUsers = (state: State) => state.draftSetup.users;
export const getLiveDraftedWrestlers = (state: State) => state.liveDraftedParticipants;
export const getLiveDraftedTeams = (state: State) => state.liveDraftedTeams;
export const getNumberOfWrestlersDrafted = (state: State) => state.liveDraftNumSelected;
export const getCurrentRoundOfDraft = (state: State) => state.liveDraftRound;
export const getStatus = (state: State) => state;
export const getTimeTillDraftInMS = (state: State) => state.timeTillDraftInMS;
export const isMyTurn = (state: State) => state.isMyTurn;
export const isDrafting = (state: State) => state.isDrafting;
export const isDraftComplete = (state: State) => state.draftSetup.draftComplete;
export const getUserDraftedTeam = (state: State, user: IDraftUser) => state.liveDraftedTeams[user.key];
export const getUserDraftedTeamByKey = (state: State, key: string) => state.liveDraftedTeams[key];
export const getCurrentDraftUser = (state: State) => {
	let users: IDraftUser[] = _.map(state.draftSetup.users, (value, key) => value);
	let clone = [...users.sort((a: IDraftUser, b: IDraftUser) => a.draftOrder - b.draftOrder)];
	let dupUsers = [...clone.slice(), ...clone.slice().reverse(), ...clone.slice()];
	let nextUp = state.liveDraftNumSelected % (users.length * 2);
	let nextUser: IDraftUser = dupUsers.slice(nextUp, nextUp + 1)[0];
	return nextUser;
}

export const getUserDraftSlot = (state: State, userId: string) => {
	let retVal = 0;
	_.map(state.draftSetup.users, (user: IDraftUser, key) => {
		if (user.id === userId) retVal = user.draftOrder;
	})
	return retVal;
}
