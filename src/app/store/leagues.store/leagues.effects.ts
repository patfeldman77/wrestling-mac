import { WrestlingService } from './../providers/wrestling.api';
import { LeagueService } from './../providers/league.api';
import { ILeague } from './../models/league.interface';
import { AppState } from './../wrestling.store';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/skip';
import 'rxjs/add/operator/takeUntil';
import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Action, Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import * as leagues from './leagues.actions';
import * as draftActions from './../draft.store/draft.actions';
import * as fromRoot from './../wrestling.store';


@Injectable()
export class LeaguesEffects {
	constructor(
		private actions$: Actions,
		private leagueApi: WrestlingService,
		private _store: Store<AppState>) { }

	@Effect()
	setLeague$ = this.actions$
		.ofType(leagues.ActionTypes.SET_CURRENT_LEAGUE)
		.switchMap(query => this.leagueApi.GetCurrentLeagueConnection(query.payload))
		.map(leagueResults => {
			this._store.dispatch(new draftActions.SubscribeToDraftAction({leagueKey: leagueResults.$key, isPrivate : leagueResults.isPrivate}));
			return new leagues.ProcessLeagueUpdateAction(leagueResults)
		});

	@Effect({ dispatch: false })
	isAdmin$ = this.actions$
		.ofType(leagues.ActionTypes.PROCESS_LEAGUE_UPDATE)
		.map(query => {
			let leagueResults: ILeague = query.payload as ILeague;
			this._store.select(fromRoot.getMyId).take(1).subscribe(id => {
				this._store.dispatch(new leagues.SetIsLeagueAdminAction(leagueResults.creatorId === id));
			})
		});

	@Effect()
	search$: Observable<Action> = this.actions$
		.ofType(leagues.ActionTypes.PUBLIC_SEARCH)
		.switchMap(query => this.leagueApi.SearchPublicLeagues(query.payload))
		.map(leagueResults => new leagues.PublicSearchCompleteAction(leagueResults));

	@Effect()
	joinLeague$: Observable<Action> = this.actions$
		.ofType(leagues.ActionTypes.JOIN_LEAGUE)
		.switchMap(query => this.leagueApi.JoinLeague(query.payload))
		.map(joinResults => new leagues.JoinLeagueCompleteAction(joinResults));

	@Effect()
	createLeague$: Observable<Action> = this.actions$
		.ofType(leagues.ActionTypes.CREATE_PUBLIC_LEAGUE)
		.switchMap(query => this.leagueApi.CreateLeague_Public(query.payload))
		.map(joinResults => new leagues.JoinLeagueCompleteAction(joinResults));

	@Effect()
	createPrivateLeague$: Observable<Action> = this.actions$
		.ofType(leagues.ActionTypes.CREATE_PRIVATE_LEAGUE)
		.switchMap(query => this.leagueApi.CreateLeague_Private(query.payload))
		.map(joinResults => new leagues.JoinLeagueCompleteAction(joinResults));

}
