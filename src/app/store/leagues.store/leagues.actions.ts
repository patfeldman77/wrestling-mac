import { IDraftUser } from './../models/user.interface';
import { IUserLeagueRef } from './../models/user-league.interface';
import { ICreateLeague } from './../models/league-create.interface';
import { ILeague } from './../models/league.interface';
import { IJoinLeague } from './../models/league-join.interface';
import { Action } from '@ngrx/store';
import { type } from '../util';
/**
 * For each action type in an action group, make a simple
 * enum object for all of this group's action types.
 * 
 * The 'type' utility function coerces strings into string
 * literal types and runs a simple check to guarantee all
 * action types in the application are unique. 
 */
export const ActionTypes = {
	PUBLIC_SEARCH: type('[LEAGUES] Public Search'),
	PUBLIC_SEARCH_COMPLETE: type('[LEAGUES] Public Search Complete'),
	SET_CURRENT_LEAGUE: type('[LEAGUES] Set Current League'),
	RESET_CURRENT_LEAGUE: type('[LEAGUES] RESet Current League'),
	SET_IS_ADMIN: type('[LEAGUES] Set Is League Admin'),
	PROCESS_LEAGUE_UPDATE: type('[LEAGUES] Process League Update'),
	JOIN_LEAGUE: type('[LEAGUES] Join League'),
	JOIN_LEAGUE_COMPLETE: type('[LEAGUES] Join League Complete'),
	CREATE_PRIVATE_LEAGUE: type('[LEAGUES] Create Private League'),
	CREATE_PRIVATE_LEAGUE_COMPLETE: type('[LEAGUES] Create Private League Complete'),
	CREATE_PUBLIC_LEAGUE: type('[LEAGUES] Create Public League'),
	CREATE_PUBLIC_LEAGUE_COMPLETE: type('[LEAGUES] Create Public League Complete'),
};

/**
 * Export a type alias of all actions in this action group
 * so that reducers can easily compose action types
 */
export type Actions =
	PublicSearchAction |
	PublicSearchCompleteAction |
	SetCurrentLeagueAction |
	ResetCurrentLeagueAction |
	SetIsLeagueAdminAction | 
	ProcessLeagueUpdateAction |
	JoinLeagueAction |
	JoinLeagueCompleteAction |
	CreatePrivateLeagueAction |
	CreatePrivateLeagueCompleteAction |
	CreatePublicLeagueAction |
	CreatePublicLeagueCompleteAction;

/**
 * Every action is comprised of at least a type and an optional
 * payload. Expressing actions as classes enables powerful 
 * type checking in reducer functions.
 * 
 * See Discriminated Unions: https://www.typescriptlang.org/docs/handbook/advanced-types.html#discriminated-unions
 */
export class PublicSearchAction implements Action {
	type = ActionTypes.PUBLIC_SEARCH;
	constructor(public payload: string) { }
}

export class PublicSearchCompleteAction implements Action {
	type = ActionTypes.PUBLIC_SEARCH_COMPLETE;
	constructor(public payload: ILeague[]) { }
}

export class SetCurrentLeagueAction implements Action {
	type = ActionTypes.SET_CURRENT_LEAGUE;
	constructor(public payload: IUserLeagueRef) { }
}

export class ResetCurrentLeagueAction implements Action {
	type = ActionTypes.RESET_CURRENT_LEAGUE;
	constructor(public payload: any=null) { }
}

export class ProcessLeagueUpdateAction implements Action {
	type = ActionTypes.PROCESS_LEAGUE_UPDATE;
	constructor(public payload: ILeague) { }
}

export class SetIsLeagueAdminAction implements Action {
	type = ActionTypes.SET_IS_ADMIN;
	constructor(public payload: boolean) { }
}

export class JoinLeagueAction implements Action {
	type = ActionTypes.JOIN_LEAGUE;
	constructor(public payload: IJoinLeague) { }
}

export class JoinLeagueCompleteAction implements Action {
	type = ActionTypes.JOIN_LEAGUE_COMPLETE;
	constructor(public payload: boolean) { }
}

export class CreatePublicLeagueAction implements Action {
	type = ActionTypes.CREATE_PUBLIC_LEAGUE;
	constructor(public payload: ICreateLeague) { }
}

export class CreatePublicLeagueCompleteAction implements Action {
	type = ActionTypes.CREATE_PUBLIC_LEAGUE_COMPLETE;
	constructor(public payload: boolean) { }
}

export class CreatePrivateLeagueAction implements Action {
	type = ActionTypes.CREATE_PRIVATE_LEAGUE;
	constructor(public payload: ICreateLeague) { }
}

export class CreatePrivateLeagueCompleteAction implements Action {
	type = ActionTypes.CREATE_PRIVATE_LEAGUE_COMPLETE;
	constructor(public payload: boolean) { }
}
