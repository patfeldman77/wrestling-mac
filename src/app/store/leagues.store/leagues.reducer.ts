import { IDraftUser } from './../models/user.interface';
import { ILeague } from './../models/league.interface';
import { IJoinLeagueStatus, eJoinStatus, IJoinLeague } from './../models/league-join.interface';
import { ICreateLeague, ICreateLeagueStatus, eCreateStatus  } from './../models/league-create.interface';
import * as actions from './leagues.actions';

export interface State {
	publicSearchResults: ILeague[];
	currentLeague: ILeague;
	isAdmin: boolean;
	joinLeagueStatus: IJoinLeagueStatus;
	createLeagueStatus: ICreateLeagueStatus;
};

const initialState: State = {
	publicSearchResults: [],
	currentLeague: null,
	isAdmin:false ,
	joinLeagueStatus: {
		status: eJoinStatus.Nothing,
		leagueName: ''
	},
	createLeagueStatus: {
		status: eCreateStatus.Nothing,
		leagueName: ''
	}
};
export function reducer(state = initialState, action: actions.Actions): State {
	switch (action.type) {
		case actions.ActionTypes.PUBLIC_SEARCH: {
			return state;
		}
		case actions.ActionTypes.PUBLIC_SEARCH_COMPLETE: {
			return Object.assign({}, state, {
				publicSearchResults: action.payload
			});
		}
		case actions.ActionTypes.RESET_CURRENT_LEAGUE:{
			return Object.assign({}, state, {
				currentLeague: null,
				isAdmin:false
			});
		}
		case actions.ActionTypes.PROCESS_LEAGUE_UPDATE:{
			let newLeague = Object.assign({}, action.payload) as ILeague;
			newLeague.$key = action.payload.$key;
			newLeague.liveDraft = null;
			return Object.assign({}, state, {
				currentLeague: newLeague
			});
		}
		case actions.ActionTypes.SET_IS_ADMIN: {
			return Object.assign({}, state, {
				isAdmin: action.payload
			})
		}
		case actions.ActionTypes.JOIN_LEAGUE: {
			const league: IJoinLeague = action.payload as IJoinLeague;
			return GetNewJoinStatus(state, eJoinStatus.Joining, league.leagueName);
		}
		case actions.ActionTypes.JOIN_LEAGUE_COMPLETE: {
			const complete: boolean = action.payload as boolean;
			return GetNewJoinStatus(state, complete ? eJoinStatus.Joined : eJoinStatus.Error);
		}
		case actions.ActionTypes.CREATE_PUBLIC_LEAGUE:
		case actions.ActionTypes.CREATE_PRIVATE_LEAGUE: {
			const createLeague: ICreateLeague = action.payload as ICreateLeague;
			return GetNewCreateStatus(state, eCreateStatus.Creating, createLeague.name);
		}
		case actions.ActionTypes.CREATE_PUBLIC_LEAGUE_COMPLETE: 
		case actions.ActionTypes.CREATE_PRIVATE_LEAGUE_COMPLETE: {
			const createComplete: boolean = action.payload as boolean;
			return GetNewCreateStatus(state, createComplete ? eCreateStatus.Created : eCreateStatus.Error);
		}
		default: {
			return state;
		}
	}
};

function GetNewJoinStatus(state: State, status: eJoinStatus, leagueName: string = null): State {
	let newState = Object.assign({}, state, {
		joinLeagueStatus: {
			status: status,
			leagueName: (!leagueName) ? state.joinLeagueStatus.leagueName : leagueName
		}
	});
	return newState;
}

function GetNewCreateStatus(state: State, status: eCreateStatus, leagueName: string = null): State {
	let newState = Object.assign({}, state, {
		createLeagueStatus: {
			status: status,
			leagueName: (!leagueName) ? state.joinLeagueStatus.leagueName : leagueName
		}
	});
	return newState;
}


export const getSelectedPublicLeagues = (state: State) => state.publicSearchResults;
export const getCurrentLeague = (state: State) => state.currentLeague;
export const getIsLeagueAdmin = (state: State) => state.isAdmin;
export const getJoinStatus = (state: State) => state.joinLeagueStatus;
export const getCreateStatus = (state: State) => state.createLeagueStatus;
