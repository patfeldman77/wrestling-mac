import { IUserLeagueRef } from './../models/user-league.interface';
import { IWrestler } from './../models/wrestlers.interface';
import { INewDraftUser, IDraftUser, IUserWithKey } from './../models/user.interface';
import { ILeague, IDraftSetup, ILiveDraft, IDraftedWrestler } from './../models/league.interface';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/zip';
import 'rxjs/add/operator/take';
import { AppState } from './../wrestling.store';
import { Store } from '@ngrx/store';
import { AngularFire, FirebaseObjectObservable } from 'angularfire2';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import * as fromRoot from './../wrestling.store';


@Injectable()
export class DraftService {
	constructor(
		private http: Http,
		private af: AngularFire,
		private _store: Store<AppState>) { }

	public AddParticipantToDraft(league: ILeague, name: string): Observable<INewDraftUser> {
		return new Observable<INewDraftUser>(observer => {
			let draftPos = 0;
			if (!!league.draftSetup.users) {
				draftPos = Object.keys(league.draftSetup.users).length;
			}
			let directive = league.isPrivate ? 'private' : 'public';
			let users = this.af.database.list('/leagues/' + directive + '/' + league.$key + '/draftSetup/users');
			let newUser = {
				name: name,
				draftOrder: draftPos
			};
			users.push(newUser).then(complete => {
				observer.next(newUser);
				observer.complete();
			}).catch(notComplete => {
				console.log("error in push new users");
				console.log(notComplete);
				observer.complete();
			});
		});
	}

	public DeleteParticipantFromDraft(league: ILeague, user: IDraftUser): Observable<IDraftUser> {
		return new Observable<IDraftUser>(observer => {
			let directive = league.isPrivate ? 'private' : 'public';
			let users = this.af.database.list('/leagues/' + directive + '/' + league.$key + '/draftSetup/users', {
				query: {
					limitToLast: 1,
					orderByChild: 'name',
					equalTo: user.name
				}
			});
			users.take(1).subscribe(foundUsers => {
				users.remove(foundUsers[0].$key)
					.then(complete => {
						observer.next(user);
						observer.complete();
					}).catch(notComplete => {
						console.log("error in push new users");
						console.log(notComplete);
						observer.complete();
					});
			});
		});
	}


	public UpdateDraftOrder(league: ILeague, newUsers: IDraftUser[]): Observable<boolean> {
		return new Observable<boolean>(observer => {
			let directive = league.isPrivate ? 'private' : 'public';
			let $dbUserList = this.af.database.list('/leagues/' + directive + '/' + league.$key + '/draftSetup/users');
			$dbUserList.take(1).subscribe(currentUsers => {
				currentUsers.forEach((value, key) => {
					let newUser = newUsers.find(user => user.name === value.name);
					if (newUser && value.draftOrder !== newUser.draftOrder) {
						$dbUserList.update(value.$key, { draftOrder: newUser.draftOrder })
							.then(update => {
								observer.next(true);
								observer.complete();
							})
							.catch(error => {
								observer.next(false); // TODO Handle this error
								observer.complete();
							});
					}
				});
			})
		});
	}

	public UpdateDraftDate(league: ILeague, date: number): Observable<boolean> {
		return new Observable<boolean>(observer => {
			let directive = league.isPrivate ? 'private' : 'public';
			let $draftDate = this.af.database.object('/leagues/' + directive + '/' + league.$key + '/draftSetup');
			$draftDate.update({draftDate: date})
				.then(update => {
					observer.next(true);
					observer.complete();
				})
				.catch(error => {
					observer.next(false); // TODO Handle this error
					observer.complete();
				});
		});
	}

	public SelectWrestler(wrestler: IWrestler) {
		// let myInfo = this._store.select(fromRoot.getMyInfo).take(1);
		// let myDraftOrder$ = this._store.select(fromRoot.getMyDraftSlot).take(1);
		// let currentLeague$ = this._store.select(fromRoot.getCurrentLeague).take(1);
		// Observable.zip(myInfo$, myDraftOrder$, currentLeague$,
		// 	(myInfo: IUserWithKey, mySlot: number, league: ILeague) => ({ myInfo, mySlot, league }))
		// 	.subscribe(combo => {
		// 		let directive = combo.league.isPrivate ? 'private' : 'public';
		// 		// update the number of people selected
		// 		let numUpdated = this.af.database.object('/leagues/' + directive + '/' + combo.league.$key + '/liveDraft/numSelected');
		// 		numUpdated.$ref.transaction(currentCount => currentCount + 1);

		// 		let draftedParticipantList = this.af.database.list('/leagues/' + directive + '/' + combo.league.$key + '/liveDraft/draftedParticipants');
		// 		let wrestlerPickInfo: IDraftedWrestler = {
		// 			wrestlerId: wrestler.id.toString(),
		// 			wrestlerWeight: wrestler.weight.toString(),
		// 			draftedRound: 1,
		// 			draftedByName: combo.myInfo.name,
		// 			draftedByKey: combo.myInfo.$key,
		// 			draftedById: combo.myInfo.id,
		// 			draftedByOrderSlot: combo.mySlot
		// 		}
		// 		draftedParticipantList.push(wrestlerPickInfo);

		// 		let myTeam = this.af.database.list('/leagues/' + directive + '/' + combo.league.$key + '/liveDraft/draftedByTeam/' + combo.myInfo.$key);
		// 		myTeam.push(wrestlerPickInfo);
		// 	})
	}
	public GetLiveDraftConnection(league: IUserLeagueRef): FirebaseObjectObservable<ILiveDraft> {
		let directive = league.isPrivate ? 'private' : 'public';
		let retVal = this.af.database.object('/leagues/' + directive + '/' + league.leagueKey + '/liveDraft');
		return retVal;
	}

	public GetDraftSetup(league: IUserLeagueRef): FirebaseObjectObservable<IDraftSetup> {
		let directive = league.isPrivate ? 'private' : 'public';
		let retVal = this.af.database.object('/leagues/' + directive + '/' + league.leagueKey + '/draftSetup');
		return retVal;
	}

}
