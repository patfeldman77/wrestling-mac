import { IWrestler } from './../models/wrestlers.interface';
import { IUserWithKey, IDraftUser, INewDraftUser } from './../models/user.interface';
import { IUserLeagueRef } from './../models/user-league.interface';
import { AppState } from './../wrestling.store';
import { Store } from '@ngrx/store';
import { ICreateLeague } from './../models/league-create.interface';
import { ILeague, IDraftedWrestler, ILiveDraft, IDraftSetup } from './../models/league.interface';
import { IJoinLeague } from './../models/league-join.interface';
import { IWrestlers } from './../models/wrestlers.interface';
import { AngularFire, FirebaseObjectObservable } from 'angularfire2';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/merge';
import 'rxjs/add/operator/bufferCount';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';

import * as fromRoot from './../wrestling.store';
import * as userActions from './../user.store/user.actions';
import * as leaguesActions from './../leagues.store/leagues.actions';

@Injectable()
export class LeagueService {
	constructor(
		private http: Http,
		private af: AngularFire,
		private _store: Store<AppState>) { }

	public GetAllParticipants(): Observable<IWrestlers> {
		return this.http.get('assets/data/participants.2015.json').map(res => res.json()).map(data => {
			return {
				wrestlersById: data.id,
				wrestlersByWeight: data.weight
			}
		});
	}

	public AddParticipantToDraft(league: ILeague, name: string): Observable<INewDraftUser> {
		return new Observable<INewDraftUser>(observer => {
			let draftPos = 0;
			if (!!league.draftSetup.users) {
				draftPos = Object.keys(league.draftSetup.users).length;
			}
			let directive = league.isPrivate ? 'private' : 'public';
			let users = this.af.database.list('/leagues/' + directive + '/' + league.$key + '/draftSetup/users');
			let newUser = {
				name: name,
				draftOrder: draftPos
			};
			users.push(newUser).then(complete => {
				observer.next(newUser);
				observer.complete();
			}).catch(notComplete => {
				console.log("error in push new users");
				console.log(notComplete);
				observer.complete();
			});
		});
	}

	public DeleteParticipantFromDraft(league: ILeague, user: IDraftUser): Observable<IDraftUser> {
		return new Observable<IDraftUser>(observer => {
			let directive = league.isPrivate ? 'private' : 'public';
			let users = this.af.database.list('/leagues/' + directive + '/' + league.$key + '/draftSetup/users', {
				query: {
					limitToLast: 1,
					orderByChild: 'name',
					equalTo: user.name
				}
			});
			users.take(1).subscribe(foundUsers => {
				users.remove(foundUsers[0].$key)
					.then(complete => {
						observer.next(user);
						observer.complete();
					}).catch(notComplete => {
						console.log("error in push new users");
						console.log(notComplete);
						observer.complete();
					});
			});
		});
	}


	public UpdateDraftOrder(league: ILeague, newUsers: IDraftUser[]): Observable<boolean> {
		return new Observable<boolean>(observer => {
			let directive = league.isPrivate ? 'private' : 'public';
			let $dbUserList = this.af.database.list('/leagues/' + directive + '/' + league.$key + '/draftSetup/users');
			$dbUserList.take(1).subscribe(currentUsers => {
				currentUsers.forEach((value, key) => {
					let newUser = newUsers.find(user => user.name === value.name);
					if (newUser && value.draftOrder !== newUser.draftOrder) {
						$dbUserList.update(value.$key, { draftOrder: newUser.draftOrder })
							.then(update => {
								observer.next(true);
								observer.complete();
							})
							.catch(error => {
								observer.next(false); // TODO Handle this error
								observer.complete();
							});
					}
				});
			})
		});
	}

	public UpdateDraftDate(league: ILeague, date: number): Observable<boolean> {
		return new Observable<boolean>(observer => {
			let directive = league.isPrivate ? 'private' : 'public';
			let $draftDate = this.af.database.object('/leagues/' + directive + '/' + league.$key + '/draftSetup');
			$draftDate.update({draftDate: date})
				.then(update => {
					observer.next(true);
					observer.complete();
				})
				.catch(error => {
					observer.next(false); // TODO Handle this error
					observer.complete();
				});
		});
	}

	public SelectWrestler(wrestler: IWrestler): void {
		let myInfo$ = this._store.select(fromRoot.getMyInfo).take(1);
		let myDraftOrder$ = this._store.select(fromRoot.getMyDraftSlot).take(1);
		let currentLeague$ = this._store.select(fromRoot.getCurrentLeague).take(1);
		Observable.zip(myInfo$, myDraftOrder$, currentLeague$,
			(myInfo: IUserWithKey, mySlot: number, league: ILeague) => ({ myInfo, mySlot, league }))
			.subscribe(combo => {
				let directive = combo.league.isPrivate ? 'private' : 'public';
				// update the number of people selected
				let numUpdated = this.af.database.object('/leagues/' + directive + '/' + combo.league.$key + '/liveDraft/numSelected');
				numUpdated.$ref.transaction(currentCount => currentCount + 1);

				let draftedParticipantList = this.af.database.list('/leagues/' + directive + '/' + combo.league.$key + '/liveDraft/draftedParticipants');
				let wrestlerPickInfo: IDraftedWrestler = {
					wrestlerId: wrestler.id.toString(),
					wrestlerWeight: wrestler.weight.toString(),
					wrestlerRank: wrestler.rank,
					draftedRound: 1,
					draftedByName: combo.myInfo.name,
					draftedByKey: combo.myInfo.$key,
					draftedById: combo.myInfo.id,
					draftedByOrderSlot: combo.mySlot
				}
				draftedParticipantList.push(wrestlerPickInfo);

				let myTeam = this.af.database.list('/leagues/' + directive + '/' + combo.league.$key + '/liveDraft/draftedByTeam/' + combo.myInfo.$key);
				myTeam.push(wrestlerPickInfo);
			})
	}
	public GetLiveDraftConnection(league: IUserLeagueRef): FirebaseObjectObservable<ILiveDraft> {
		let directive = league.isPrivate ? 'private' : 'public';
		let retVal = this.af.database.object('/leagues/' + directive + '/' + league.leagueKey + '/liveDraft');
		return retVal;
	}

	public GetDraftSetup(league: IUserLeagueRef): FirebaseObjectObservable<IDraftSetup> {
		let directive = league.isPrivate ? 'private' : 'public';
		let retVal = this.af.database.object('/leagues/' + directive + '/' + league.leagueKey + '/draftSetup');
		return retVal;
	}

	public GetCurrentLeagueConnection(league: IUserLeagueRef): Observable<ILeague> {
		let directive = league.isPrivate ? 'private' : 'public';
		return this.af.database.object('/leagues/' + directive + '/' + league.leagueKey).take(1);
	}
	// USER FUNCTIONS
	public AddLeagueToCurrentUser(leagueRef: IUserLeagueRef): Observable<boolean> {
		let retVal = new Observable<boolean>(observer => {
			this._store.select(fromRoot.getMyInfo).take(1).subscribe(myInfo => {
				let myLeagues = this.af.database.list('/users/' + myInfo.$key + '/leagues_ref');
				myLeagues.push(leagueRef)
					.then(leagueInfo => {
						observer.next(true);
						observer.complete();
					})
					.catch(error => {
						observer.next(false);
						observer.complete();
					});
			});
		});
		return retVal;
	}

	public SearchPublicLeagues(searchString: string): Observable<ILeague[]> {
		return this.af.database.list('/leagues/public', {
			query: {
				limitToLast: 10,
				orderByChild: 'isFull',
				equalTo: false
			}
		});
	}
	public JoinLeague(leagueInfo: IJoinLeague): Observable<boolean> {
		if (!leagueInfo.isPrivate) {
			let retVal = new Observable<boolean>(observer => {
				this.JoinLeague_API({
					leagueKey: leagueInfo.leagueKey,
					isPrivate: leagueInfo.isPrivate,
					leagueName: leagueInfo.leagueName,
					draftDate: leagueInfo.leagueDraftDate,
					creator: leagueInfo.leagueCreator,
					creatorId: leagueInfo.leagueCreatorId
				}, observer);
			});
			return retVal;
		} else {
			let retVal = new Observable<boolean>(observer => {
				this.af.database.list('/leagues/private', {
					query: {
						limitToLast: 1,
						orderByChild: 'name',
						equalTo: leagueInfo.leagueName
					}
				}).take(1).subscribe(leagues => {
					if ((leagues.length === 0) || (leagues[0].password !== leagueInfo.leaguePassword)) {
						observer.next(false);
						observer.complete();
					} else {
						this.JoinLeague_API({
							leagueKey: leagues[0].$key,
							isPrivate: true,
							leagueName: leagues[0].name,
							draftDate: leagues[0].draftSetup.draftDate,
							creator: leagues[0].creator,
							creatorId: leagues[0].creatorId
						}, observer);
					}
				});
			});
			return retVal;
		}
	}

	private JoinLeague_API(leagueInfo: IUserLeagueRef, observer: Observer<boolean>): void {
		this._store.select(fromRoot.getMyInfo).take(1).subscribe(myInfo => {
			let fork = leagueInfo.isPrivate ? 'private' : 'public';
			let leagueDB = this.af.database.object('/leagues/' + fork + '/' + leagueInfo.leagueKey);
			leagueDB.skipWhile(data => !data).take(1).subscribe(league => {
				let leagueUsers = this.af.database.list('/leagues/' + fork + '/' + leagueInfo.leagueKey + '/draftSetup/users');
				let count: number = (!league.draftSetup || !league.draftSetup.users) ? 0 : Object.keys(league.draftSetup.users).length; // DON'T ADD 1 because the prototype of the object is a key
				leagueUsers.push({
					id: myInfo.id,
					key: myInfo.$key,
					name: myInfo.name,
					email: myInfo.email,
					draftOrder: count
				}).then(pushedInfo => {
					this._store.dispatch(new userActions.AddUserLeagueAction(leagueInfo));
					observer.next(true);
					observer.complete();
				}).catch(error => {
					console.warn(error);
					observer.next(false);
					observer.complete();
				});
			});
		});
	}

	public CreateLeague_Public(league: ICreateLeague): Observable<boolean> {
		return this.CreateLeague_Generic(league, false);
	}
	public CreateLeague_Private(league: ICreateLeague): Observable<boolean> {
		return this.CreateLeague_Generic(league, true);
	}

	private CreateLeague_Generic(league: ICreateLeague, isPrivate: boolean): Observable<boolean> {
		let retVal = new Observable<boolean>(observer => {
			this._store.select(fromRoot.getMyInfo).take(1).subscribe(myInfo => {
				let newLeague = {
					name: league.name,
					numUsers: 1,
					maxUsers: league.maxUsers,
					isFull: false,
					createDate: league.createDate,
					isPrivate: isPrivate,
					draftSetup: {
						draftDate: Date.parse("March 14, 2017")
					},
					liveDraft: {
						numSelected: 0
					},
					password: league.password,
					creator: myInfo.name,
					creatorKey: myInfo.$key,
					creatorId: myInfo.id
				}
				let fork = isPrivate ? 'private' : 'public';
				let $leagues = this.af.database.list('/leagues/' + fork);
				$leagues.push(newLeague).then((createdLeague) => {
					let joinLeague: IJoinLeague = {
						leagueName: league.name,
						leaguePassword: league.password,
						leagueDraftDate: league.draftDate,
						leagueKey: createdLeague.key,
						leagueCreator: myInfo.name,
						leagueCreatorId: myInfo.id,
						isPrivate: isPrivate
					}
					this._store.dispatch(new leaguesActions.JoinLeagueAction(joinLeague))
					observer.next(true);
					observer.complete();
				}).catch(error => {
					observer.next(false);
					observer.complete();
				});
			})
		});
		return retVal;
	}
}
