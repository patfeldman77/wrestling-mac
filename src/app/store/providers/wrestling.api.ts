import { ScoreConvert } from './../../util/score.convert';
import { IWrestlerHighlight } from './../../info/highlights.class';
import { weights } from './../../info/weights';
import { IWrestler } from './../models/wrestlers.interface';
import { IUserWithKey, IDraftUser, INewDraftUser } from './../models/user.interface';
import { IUserLeagueRef } from './../models/user-league.interface';
import { AppState } from './../wrestling.store';
import { Store } from '@ngrx/store';
import { ICreateLeague } from './../models/league-create.interface';
import { ILeague, IDraftedWrestler, ILiveDraft, IDraftSetup } from './../models/league.interface';
import { IJoinLeague } from './../models/league-join.interface';
import { IWrestlers } from './../models/wrestlers.interface';
import { AngularFire, FirebaseObjectObservable } from 'angularfire2';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/merge';
import 'rxjs/add/operator/bufferCount';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import * as _ from 'lodash';
import * as fromRoot from './../wrestling.store';
import * as userActions from './../user.store/user.actions';
import * as leaguesActions from './../leagues.store/leagues.actions';
import * as wrestlersActions from '../wrestlers.store/wrestlers.actions';

@Injectable()
export class WrestlingService {
	constructor(
		private http: Http,
		private af: AngularFire,
		private _store: Store<AppState>) { }


	public GetAllParticipants(): Observable<IWrestlers> {
		return this.http.get('assets/data/participants.2017.scores.json').map(res => res.json())
			.map(data => {
				data.id.forEach((wrestler, index, array) => {
					wrestler.id = index;
				})
				data.weight = {};
				weights.forEach(weight => {
					data.weight[weight] = _.map(data.id, (value, key) => value)
						.filter(wrestler => {
							return !!wrestler && wrestler.weight == weight;
						}).sort((a: IWrestler, b: IWrestler) => a.rank - b.rank).map(wrestler => wrestler.id);
				});
				return {
					wrestlersById: data.id,
					wrestlersByWeight: data.weight
				}
			});
		// return this.http.get('assets/data/participants.2015.json').map(res => res.json()).map(data => {
		// 	return {
		// 		wrestlersById: data.id,
		// 		wrestlersByWeight: data.weight
		// 	}
		// });
	}

	public AddWrestlerToMyHighlights(myKey: string, wrestler: IWrestler, highlightId: number, priority: number): Observable<IWrestlerHighlight> {
		return new Observable<IWrestlerHighlight>(observer => {
			let myHighlights = this.af.database.list('/users/' + myKey + '/highlights');
			let highlightedWrestler: IWrestlerHighlight = {
				wrestlerId: wrestler.id,
				wrestlerWeight: wrestler.weight,
				priority: priority,
				highlightId: highlightId, 
				key: ''
			};
			myHighlights.push(highlightedWrestler).then(complete => {
				highlightedWrestler.key = complete.key;
				observer.next(highlightedWrestler);
				observer.complete();
			}).catch(error => {
				observer.complete();
			});
		});
	}
	public RemoveWrestlerToMyHighlights(myKey, wrestlerId): Observable<number> {
		return new Observable<number>(observer => {
			let myHighlights = this.af.database.list('/users/' + myKey + '/highlights', {
				query: {
					limitToLast: 1,
					orderByChild: 'wrestlerId',
					equalTo: wrestlerId
				}
			});
			myHighlights.take(1).subscribe(foundHighlight => {
				if (!foundHighlight || foundHighlight.length === 0) {
					observer.complete();
				} else {
					myHighlights.remove(foundHighlight[0].$key)
						.then(complete => {
							observer.next(wrestlerId);
							observer.complete();
						}).catch(notComplete => {
							observer.complete();
						});
				}
			});
		});
	}

	public AddParticipantToDraft(league: ILeague, name: string): Observable<INewDraftUser> {
		return new Observable<INewDraftUser>(observer => {
			let draftPos = 0;
			if (!!league.draftSetup.users) {
				draftPos = Object.keys(league.draftSetup.users).length;
			}
			let directive = league.isPrivate ? 'private' : 'public';
			let users = this.af.database.list('/leagues/' + directive + '/' + league.$key + '/draftSetup/users');
			let newUser = {
				name: name,
				draftOrder: draftPos
			};
			users.push(newUser).then(complete => {
				observer.next(newUser);
				observer.complete();
			}).catch(notComplete => {
				console.log("error in push new users");
				console.log(notComplete);
				observer.complete();
			});
		});
	}

	public DeleteParticipantFromDraft(league: ILeague, user: IDraftUser): Observable<IDraftUser> {
		return new Observable<IDraftUser>(observer => {
			let directive = league.isPrivate ? 'private' : 'public';
			let users = this.af.database.list('/leagues/' + directive + '/' + league.$key + '/draftSetup/users', {
				query: {
					limitToLast: 1,
					orderByChild: 'name',
					equalTo: user.name
				}
			});
			users.take(1).subscribe(foundUsers => {
				users.remove(foundUsers[0].$key)
					.then(complete => {
						observer.next(user);
						observer.complete();
					}).catch(notComplete => {
						console.log("error in push new users");
						console.log(notComplete);
						observer.complete();
					});
			});
		});
	}


	public UpdateDraftOrder(league: ILeague, newUsers: IDraftUser[]): Observable<boolean> {
		return new Observable<boolean>(observer => {
			let directive = league.isPrivate ? 'private' : 'public';
			let $dbUserList = this.af.database.list('/leagues/' + directive + '/' + league.$key + '/draftSetup/users');
			$dbUserList.take(1).subscribe(currentUsers => {
				currentUsers.forEach((value, key) => {
					let newUser = newUsers.find(user => user.name === value.name);
					if (newUser && value.draftOrder !== newUser.draftOrder) {
						$dbUserList.update(value.$key, { draftOrder: newUser.draftOrder })
							.then(update => {
								observer.next(true);
								observer.complete();
							})
							.catch(error => {
								observer.next(false); // TODO Handle this error
								observer.complete();
							});
					}
				});
			})
		});
	}

	public UpdateDraftDate(league: ILeague, date: number): Observable<boolean> {
		return new Observable<boolean>(observer => {
			let directive = league.isPrivate ? 'private' : 'public';
			let $draftDate = this.af.database.object('/leagues/' + directive + '/' + league.$key + '/draftSetup');
			$draftDate.update({ draftDate: date })
				.then(update => {
					observer.next(true);
					observer.complete();
				})
				.catch(error => {
					observer.next(false); // TODO Handle this error
					observer.complete();
				});
		});
	}
	public SelectWrestler(wrestler: IWrestler, draftedBy: IUserWithKey, leagueKey: string, isPrivate: boolean, round: number, slot: number): void {
		let directive = isPrivate ? 'private' : 'public';
		// update the number of people selected
		let numUpdated = this.af.database.object('/leagues/' + directive + '/' + leagueKey + '/liveDraft/numSelected');
		numUpdated.$ref.transaction(currentCount => currentCount + 1);

		let draftedParticipantList = this.af.database.list('/leagues/' + directive + '/' + leagueKey + '/liveDraft/draftedParticipants');
		let wrestlerPickInfo: IDraftedWrestler = {
			wrestlerId: wrestler.id.toString(),
			wrestlerWeight: wrestler.weight.toString(),
			wrestlerRank: wrestler.rank,
			draftedRound: round,
			draftedByName: draftedBy.name,
			draftedByKey: draftedBy.$key,
			draftedById: draftedBy.id,
			draftedByOrderSlot: slot
		}
		draftedParticipantList.push(wrestlerPickInfo);
		let myTeam = this.af.database.list('/leagues/' + directive + '/' + leagueKey + '/liveDraft/draftedByTeam/' + draftedBy.$key);
		myTeam.push(wrestlerPickInfo);

	};

	public GetLiveDraftNumSelectedConnection(leagueKey: string, isPrivate: boolean): FirebaseObjectObservable<any> {
		let directive = isPrivate ? 'private' : 'public';
		let retVal = this.af.database.object('/leagues/' + directive + '/' + leagueKey + '/liveDraft/numSelected');
		return retVal;
	}

	public GetLiveDraftTeamsConnection(leagueKey: string, isPrivate: boolean): FirebaseObjectObservable<dictionary<dictionary<IDraftedWrestler>>> {
		let directive = isPrivate ? 'private' : 'public';
		let retVal = this.af.database.object('/leagues/' + directive + '/' + leagueKey + '/liveDraft/draftedByTeam');
		return retVal;
	}

	public GetLiveDraftParticipantsConnection(leagueKey: string, isPrivate: boolean): FirebaseObjectObservable<dictionary<IDraftedWrestler>> {
		let directive = isPrivate ? 'private' : 'public';
		let retVal = this.af.database.object('/leagues/' + directive + '/' + leagueKey + '/liveDraft/draftedParticipants');
		return retVal;
	}

	public GetLiveDraftConnection(leagueKey: string, isPrivate: boolean): FirebaseObjectObservable<ILiveDraft> {
		let directive = isPrivate ? 'private' : 'public';
		let retVal = this.af.database.object('/leagues/' + directive + '/' + leagueKey + '/liveDraft');
		return retVal;
	}

	public GetDraftSetup(leagueKey: string, isPrivate: boolean): FirebaseObjectObservable<IDraftSetup> {
		let directive = isPrivate ? 'private' : 'public';
		let retVal = this.af.database.object('/leagues/' + directive + '/' + leagueKey + '/draftSetup');
		return retVal;
	}

	public GetCurrentLeagueConnection(league: IUserLeagueRef): Observable<ILeague> {
		let directive = league.isPrivate ? 'private' : 'public';
		return this.af.database.object('/leagues/' + directive + '/' + league.leagueKey).take(1);
	}
	// USER FUNCTIONS
	public AddLeagueToCurrentUser(leagueRef: IUserLeagueRef): Observable<boolean> {
		let retVal = new Observable<boolean>(observer => {
			this._store.select(fromRoot.getMyInfo).take(1).subscribe(myInfo => {
				let myLeagues = this.af.database.list('/users/' + myInfo.$key + '/leagues_ref');
				myLeagues.push(leagueRef)
					.then(leagueInfo => {
						observer.next(true);
						observer.complete();
					})
					.catch(error => {
						observer.next(false);
						observer.complete();
					});
			});
		});
		return retVal;
	}

	public SearchPublicLeagues(searchString: string): Observable<ILeague[]> {
		return this.af.database.list('/leagues/public', {
			query: {
				limitToLast: 10,
				orderByChild: 'isFull',
				equalTo: false
			}
		});
	}
	public JoinLeague(leagueInfo: IJoinLeague): Observable<boolean> {
		if (!leagueInfo.isPrivate) {
			let retVal = new Observable<boolean>(observer => {
				this.JoinLeague_API({
					leagueKey: leagueInfo.leagueKey,
					isPrivate: leagueInfo.isPrivate,
					leagueName: leagueInfo.leagueName,
					draftDate: leagueInfo.leagueDraftDate,
					creator: leagueInfo.leagueCreator,
					creatorId: leagueInfo.leagueCreatorId
				}, observer);
			});
			return retVal;
		} else {
			let retVal = new Observable<boolean>(observer => {
				this.af.database.list('/leagues/private', {
					query: {
						limitToLast: 1,
						orderByChild: 'name',
						equalTo: leagueInfo.leagueName
					}
				}).take(1).subscribe(leagues => {
					if ((leagues.length === 0) || (leagues[0].password !== leagueInfo.leaguePassword)) {
						observer.next(false);
						observer.complete();
					} else {
						this.JoinLeague_API({
							leagueKey: leagues[0].$key,
							isPrivate: true,
							leagueName: leagues[0].name,
							draftDate: leagues[0].draftSetup.draftDate,
							creator: leagues[0].creator,
							creatorId: leagues[0].creatorId
						}, observer);
					}
				});
			});
			return retVal;
		}
	}

	private JoinLeague_API(leagueInfo: IUserLeagueRef, observer: Observer<boolean>): void {
		this._store.select(fromRoot.getMyInfo).take(1).subscribe(myInfo => {
			let fork = leagueInfo.isPrivate ? 'private' : 'public';
			let leagueDB = this.af.database.object('/leagues/' + fork + '/' + leagueInfo.leagueKey);
			leagueDB.skipWhile(data => !data).take(1).subscribe(league => {
				let leagueUsers = this.af.database.list('/leagues/' + fork + '/' + leagueInfo.leagueKey + '/draftSetup/users');
				let count: number = (!league.draftSetup || !league.draftSetup.users) ? 0 : Object.keys(league.draftSetup.users).length; // DON'T ADD 1 because the prototype of the object is a key
				leagueUsers.push({
					id: myInfo.id,
					key: myInfo.$key,
					name: myInfo.name,
					email: myInfo.email,
					photo: myInfo.photo,
					draftOrder: count
				}).then(pushedInfo => {
					this._store.dispatch(new userActions.AddUserLeagueAction(leagueInfo));
					observer.next(true);
					observer.complete();
				}).catch(error => {
					console.warn(error);
					observer.next(false);
					observer.complete();
				});
			});
		});
	}

	public CreateLeague_Public(league: ICreateLeague): Observable<boolean> {
		return this.CreateLeague_Generic(league, false);
	}
	public CreateLeague_Private(league: ICreateLeague): Observable<boolean> {
		return this.CreateLeague_Generic(league, true);
	}

	private CreateLeague_Generic(league: ICreateLeague, isPrivate: boolean): Observable<boolean> {
		let retVal = new Observable<boolean>(observer => {
			this._store.select(fromRoot.getMyInfo).take(1).subscribe(myInfo => {
				let newLeague = {
					name: league.name,
					numUsers: 1,
					maxUsers: league.maxUsers,
					isFull: false,
					createDate: league.createDate,
					isPrivate: isPrivate,
					draftSetup: {
						draftDate: Date.parse("March 14, 2017")
					},
					liveDraft: {
						numSelected: 0
					},
					password: league.password,
					creator: myInfo.name,
					creatorKey: myInfo.$key,
					creatorId: myInfo.id
				}
				let fork = isPrivate ? 'private' : 'public';
				let $leagues = this.af.database.list('/leagues/' + fork);
				$leagues.push(newLeague).then((createdLeague) => {
					let joinLeague: IJoinLeague = {
						leagueName: league.name,
						leaguePassword: league.password,
						leagueDraftDate: league.draftDate,
						leagueKey: createdLeague.key,
						leagueCreator: myInfo.name,
						leagueCreatorId: myInfo.id,
						isPrivate: isPrivate
					}
					this._store.dispatch(new leaguesActions.JoinLeagueAction(joinLeague))
					observer.next(true);
					observer.complete();
				}).catch(error => {
					observer.next(false);
					observer.complete();
				});
			})
		});
		return retVal;
	}
}
