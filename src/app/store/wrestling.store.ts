import { DraftEffects } from './draft.store/draft.effects';
import { LeaguesEffects } from './leagues.store/leagues.effects';
import { UserEffects } from './user.store/user.effects';
import { WrestlersEffects } from './wrestlers.store/wrestlers.effects';
import { EffectsModule } from '@ngrx/effects';
import { createSelector } from 'reselect';
import '@ngrx/core/add/operator/select';
import { combineReducers, ActionReducer } from '@ngrx/store';


import * as fromWrestlers from './wrestlers.store/wrestlers.reducer';
import * as fromUser from './user.store/user.reducer';
import * as fromLeague from './leagues.store/leagues.reducer';
import * as fromDraft from './draft.store/draft.reducer';

export const RunEffects = [
	EffectsModule.runAfterBootstrap(WrestlersEffects),
	EffectsModule.runAfterBootstrap(UserEffects),
	EffectsModule.runAfterBootstrap(LeaguesEffects),
	EffectsModule.runAfterBootstrap(DraftEffects)
]
export interface AppState {
	currentUser: fromUser.State;
	leagues: fromLeague.State;
	draft: fromDraft.State;
	wrestlers: fromWrestlers.State;
};

const reducers = {
	currentUser: fromUser.reducer,
	leagues: fromLeague.reducer ,
	draft: fromDraft.reducer,
	wrestlers: fromWrestlers.reducer
}

const productionReducer: ActionReducer<AppState> = combineReducers(reducers);

export function reducer(state: any, action: any) {
    return productionReducer(state, action);
}


// The store selects 
export const getDraft = (state: AppState) => state.draft;
export const getDraftSetup = createSelector(getDraft, fromDraft.getDraftSetup);
export const getLiveDraftedWrestlers = createSelector(getDraft, fromDraft.getLiveDraftedWrestlers);
export const getLiveDraftedTeams = createSelector(getDraft, fromDraft.getLiveDraftedTeams);
export const getNumberOfWrestlersDrafted = createSelector(getDraft, fromDraft.getNumberOfWrestlersDrafted);
export const getCurrentRound = createSelector(getDraft, fromDraft.getCurrentRoundOfDraft);
export const getTimeTillDraftInMS = createSelector(getDraft, fromDraft.getTimeTillDraftInMS);
export const getDraftUsers = createSelector(getDraft, fromDraft.getDraftUsers);
export const isMyTurn = createSelector(getDraft, fromDraft.isMyTurn);
export const isDrafting = createSelector(getDraft, fromDraft.isDrafting);
export const isDraftComplete = createSelector(getDraft, fromDraft.isDraftComplete);
export const getCurrentDraftUser = createSelector( getDraft, fromDraft.getCurrentDraftUser);
export const getCurrentDraftTeam = createSelector( getDraft, getCurrentDraftUser, fromDraft.getUserDraftedTeam);
// export const getUserDraftedTeam = (state: State, userKey: string) => state.liveDraftedTeams[userKey];

export const getLeagues = (state: AppState) => state.leagues;
export const getPublicLeagues = createSelector(getLeagues, fromLeague.getSelectedPublicLeagues);
export const getCurrentLeague = createSelector(getLeagues, fromLeague.getCurrentLeague);
export const getJoinLeagueStatus = createSelector(getLeagues, fromLeague.getJoinStatus);
export const isCurrentLeagueAdmin = createSelector(getLeagues, fromLeague.getIsLeagueAdmin);

export const getUser = (state: AppState) => state.currentUser;
export const getAuthenticatedState = createSelector (getUser, fromUser.getAuthenticatedState);
export const getMyLeagues = createSelector (getUser, fromUser.getMyLeagues);
export const getMyInfo = createSelector (getUser, fromUser.getMyInfo);
export const getMyId = createSelector (getUser, fromUser.getMyId);
export const getMyKey = createSelector (getUser, fromUser.getMyKey);
export const getMyHighlights = createSelector( getUser, fromUser.getMyHighlights);
export const getMyDraftedTeam = createSelector(getDraft, getMyKey, fromDraft.getUserDraftedTeamByKey);
export const getMyDraftSlot = createSelector(getDraft, getMyId, fromDraft.getUserDraftSlot);

export const getWrestlers = (state: AppState) => state.wrestlers;
export const getAllWrestlers = createSelector(getWrestlers, fromWrestlers.getWrestlers )
export const getAllWeights = createSelector (getWrestlers, fromWrestlers.getAllWeights);
export const getAllWrestlersByWeight = createSelector(getWrestlers, fromWrestlers.getAllWrestlersByWeight);
export const getAllWrestlersById = createSelector(getWrestlers, fromWrestlers.getWrestlersById );
export const getSelectedWrestler = createSelector(getWrestlers, fromWrestlers.getSelectedWrestler);
export const getWrestlersAtSelectedWeight = createSelector(getWrestlers, fromWrestlers.getWrestlersAtSelectedWeight );