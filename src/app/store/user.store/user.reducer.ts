import { IWrestlerHighlight } from './../../info/highlights.class';
import { IUserWithKey } from './../models/user.interface';
import { ELogin } from './../models/login.enum';
import * as actions from './user.actions';
import * as _ from 'lodash';
export interface State {
	authState: ELogin;
	myInfo: IUserWithKey;
} ;

const initialState: State = {
	authState: ELogin.NotSet,
	myInfo: {
		name: "",
		photo: "", 
		email: "",
		id: "",
		$key: "",
		leagues_ref:{}, 
		highlights: []
	}
};
export function reducer(state = initialState, action: actions.Actions): State {
	switch (action.type) {
		case actions.ActionTypes.SET_USER: {
			let myNewInfo = Object.assign({}, action.payload) as IUserWithKey;
			myNewInfo.$key = action.payload.$key;
			myNewInfo.photo = (!!action.payload.photo) ? action.payload.photo : '';
			myNewInfo.highlights = _.map(action.payload.highlights, (value, key) =>{
				return Object.assign({}, value, {key: key});
			});

			return Object.assign({}, state, {
				authState : ELogin.LoggedIn,
				myInfo: myNewInfo
			});
		}
		case actions.ActionTypes.SET_NO_USER: {
			return Object.assign({}, state, {
				authState : ELogin.NoUser
			});
		}
		case actions.ActionTypes.LOGOUT_USER: {
			return Object.assign({}, initialState, {
				authState: ELogin.NoUser
			});
		}
		case actions.ActionTypes.ADD_USER_LEAGUE :{
			let newLeagueObj = (!state.myInfo.leagues_ref) ? {} : Object.assign({}, state.myInfo.leagues_ref);
			newLeagueObj[action.payload.leagueKey] = action.payload;
			let newInfo: IUserWithKey = Object.assign({}, state.myInfo, {
				leagues_ref: newLeagueObj
			});
			return Object.assign({}, state, {
				myInfo: newInfo
			});
		}
		case actions.ActionTypes.ADD_WRESTLER_HIGHLIGHT_COMPLETE : {
			let newHighlights: IWrestlerHighlight[] = [...state.myInfo.highlights, action.payload];
			let newMyInfo = Object.assign({}, state.myInfo, {
				highlights: newHighlights
			})
			return Object.assign({}, state, {
				myInfo: newMyInfo
			});
		}
		case actions.ActionTypes.REMOVE_WRESTLER_HIGHLIGHT_COMPLETE : {
			let newHighlights: IWrestlerHighlight[] = [...state.myInfo.highlights]
				.filter(highlight => highlight.wrestlerId !== action.payload);
			let newMyInfo = Object.assign({}, state.myInfo, {
				highlights: newHighlights
			})
			return Object.assign({}, state, {
				myInfo: newMyInfo
			});
		}
		default: {
			return state;
		}
	}	
};

export const getMyLeagues = (state: State) => state.myInfo.leagues_ref;
export const getMyInfo = (state: State) => state.myInfo;
export const getMyId = (state: State) => state.myInfo.id;
export const getMyKey = (state: State) => state.myInfo.$key;
export const getMyHighlights = (state: State) => state.myInfo.highlights;
export const getAuthenticatedState = (state: State) => state.authState;