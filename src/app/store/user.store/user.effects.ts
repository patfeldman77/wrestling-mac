import { IWrestlerHighlight } from './../../info/highlights.class';
import { WrestlingService } from './../providers/wrestling.api';
import { LeagueService } from './../providers/league.api';
import { Platform } from 'ionic-angular';
import { Facebook, GooglePlus } from 'ionic-native';
import { IUser, IUserWithKey } from './../models/user.interface';
import { AuthProviders, AuthMethods, AngularFire, FirebaseAuthState } from 'angularfire2';
import * as firebase from 'firebase';
import { AppState } from './../wrestling.store';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/skip';
import 'rxjs/add/operator/takeUntil';
import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Store } from '@ngrx/store';

import * as userActions from './user.actions';
import * as fromRoot from './../wrestling.store';


@Injectable()
export class UserEffects {
	private authState: FirebaseAuthState;
	constructor(
		private actions$: Actions,
		private leagueApi: WrestlingService,
		private af: AngularFire,
		private _store: Store<AppState>,
		private platform: Platform
	) {
		this.af.auth.subscribe(firebaseState => {
			this.authState = firebaseState;
			if (this.authState !== null) {
				this.Login();
			} else {
				// logout
				this._store.dispatch(new userActions.SetNoUserAction());
			}
		});
	}


	private Login() {
		let isMocking = false;
		let mockAuthId = "EU6jbQ9t2hVnKlCW2TMkb8rXvCy2";
		let authId = (isMocking) ? mockAuthId : this.authState.uid;

		this.af.database.list('/users', {
			query: {
				limitToLast: 1,
				orderByChild: 'id',
				equalTo: authId
			}
		}).first().subscribe(user => {
			if (user.length === 0) {
				let myName = this.authState.auth.displayName;
				myName = (!!myName ? myName : this.authState.auth.email.split('@')[0]);
				let me: IUser = {
					id: this.authState.uid,
					leagues_ref: {},
					highlights: [],
					name: myName,
					photo: this.authState.auth.photoURL,
					email: this.authState.auth.email
				};
				const users = this.af.database.list('/users');
				users.push(me).then((item) => {
					let meWithKey: IUserWithKey = {
						id: me.id,
						leagues_ref: me.leagues_ref,
						highlights: me.highlights,
						name: me.name,
						photo: me.photo,
						email: me.email,
						$key: item.key
					}
					this._store.dispatch(new userActions.SetUserAction(meWithKey));
				});
			} else {
				let meWithKey: IUserWithKey = user[0];
				this._store.dispatch(new userActions.SetUserAction(meWithKey));
			}
		})
	}

	@Effect({ dispatch: false })
	fbLogin$ = this.actions$
		.ofType(userActions.ActionTypes.LOGIN_USER_FACEBOOK)
		.map(query => {
			if (this.platform.is('cordova')) {
				return Facebook.login(['email', 'public_profile']).then(res => {
					const facebookCredential = firebase.auth.FacebookAuthProvider.credential(res.authResponse.accessToken);
					firebase.auth().signInWithCredential(facebookCredential).then(success => { console.log("success FB-fire signin"); console.log(success); }).catch(failure => { console.log("fail FB-fire signin"); console.log(failure); });
				}).catch(failure => {
					console.log("failure FB login");
					console.log(failure);
				});

			} else {
				this.af.auth.login({
					provider: AuthProviders.Facebook,
					method: AuthMethods.Popup,
				}).then(newAuthState => {
					console.log("SUCCESS FB login");
				}).catch(failure => {
					console.log("failure FB login");
					console.log(failure);
				});
			}
		});

	@Effect({ dispatch: false })
	googleLogin$ = this.actions$
		.ofType(userActions.ActionTypes.LOGIN_USER_GOOGLE)
		.map(query => {
			if (this.platform.is('cordova')) {
				GooglePlus.login({
					'webClientId': '228254700561-9arv24simauqhdchfddlkdvduurer3vp.apps.googleusercontent.com'
				}).then((userData) => {
					const googleCredential = firebase.auth.GoogleAuthProvider.credential(userData.idToken);
					firebase.auth().signInWithCredential(googleCredential).then(success => { console.log("success G+-fire signin"); console.log(success); }).catch(failure => { console.log("fail G+-fire signin"); console.log(failure); });
				}).catch(failure => {
					console.log("failure G+ login");
					console.log(failure);
				});
			} else {
				this.af.auth.login({
					provider: AuthProviders.Google,
					method: AuthMethods.Popup,
				}).then(newAuthState => {
					console.log("SUCCESS G+ login");
				}).catch(failure => {
					console.log("failure G+ login");
					console.log(failure);
				});
			}
		});

	@Effect({ dispatch: false })
	logout$ = this.actions$
		.ofType(userActions.ActionTypes.LOGOUT_USER)
		.map(query => { this.af.auth.logout() });

	@Effect()
	newLeague$ = this.actions$
		.ofType(userActions.ActionTypes.ADD_USER_LEAGUE)
		.switchMap(query => this.leagueApi.AddLeagueToCurrentUser(query.payload))
		.map(complete => new userActions.AddUserLeagueCompleteAction(complete));

	@Effect()
	addHighlight$ = this.actions$
		.ofType(userActions.ActionTypes.ADD_WRESTLER_HIGHLIGHT)
		.switchMap(query => this._store.select(fromRoot.getMyKey).take(1), (query, key) => ({ query: query, key: key }))
		.switchMap(keyAndQuery => {
			let priority = 1;
			return this.leagueApi.AddWrestlerToMyHighlights(keyAndQuery.key, keyAndQuery.query.payload.wrestler, keyAndQuery.query.payload.highlight.id, priority)
		})
		.map(wrestlerWithKey => new userActions.AddWrestlerHighlightCompleteAction(wrestlerWithKey));

	@Effect()
	removeHighlight$ = this.actions$
		.ofType(userActions.ActionTypes.REMOVE_WRESTLER_HIGHLIGHT)
		.switchMap(query => this._store.select(fromRoot.getMyKey).take(1), (query, key) => ({ query: query, key: key }))
		.switchMap(keyAndQuery => this.leagueApi.RemoveWrestlerToMyHighlights(keyAndQuery.key, keyAndQuery.query.payload.id))
		.map(wrestlerId => new userActions.RemoveWrestlerHighlightCompleteAction(wrestlerId));
}
