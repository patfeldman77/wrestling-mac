import { IHighlightDetails, IWrestlerHighlight } from './../../info/highlights.class';
import { IWrestler } from './../models/wrestlers.interface';
import { IUserLeagueRef } from './../models/user-league.interface';
import { IUserWithKey } from './../models/user.interface';
import { Action } from '@ngrx/store';
import { type } from '../util';
/**
 * For each action type in an action group, make a simple
 * enum object for all of this group's action types.
 * 
 * The 'type' utility function coerces strings into string
 * literal types and runs a simple check to guarantee all
 * action types in the application are unique. 
 */
export const ActionTypes = {
	LOGIN_USER_FACEBOOK: type('[USER] Login User Facebook'),
	LOGIN_USER_GOOGLE: type('[USER] Login User Google'),
	LOGOUT_USER: type('[USER] Logout User'),
	SET_USER: type('[USER] Set User'),
	SET_NO_USER: type('[USER] Set NO User'),
	ADD_USER_LEAGUE: type('[USER] Add User League'),
	ADD_USER_LEAGUE_COMPLETE: type('[USER] Add User League Complete'),
	ADD_WRESTLER_HIGHLIGHT: type('[USER] Add Wrestler Highlight'),
	ADD_WRESTLER_HIGHLIGHT_COMPLETE: type('[USER] Add Wrestler Highlight Complete'),
	REMOVE_WRESTLER_HIGHLIGHT: type('[USER] Remove Wrestler Highlight'),
	REMOVE_WRESTLER_HIGHLIGHT_COMPLETE: type('[USER] Remove Wrestler Highlight Complete'),
};

/**
 * Export a type alias of all actions in this action group
 * so that reducers can easily compose action types
 */
export type Actions =
	LoginUserGoogleAction |
	LoginUserFacebookAction |
	LogoutUserAction | 
	SetUserAction | 
	SetNoUserAction | 
	AddUserLeagueAction |
	AddUserLeagueCompleteAction |
	AddWrestlerHighlightAction |
	AddWrestlerHighlightCompleteAction |
	RemoveWrestlerHighlightAction |
	RemoveWrestlerHighlightCompleteAction ;

/**
 * Every action is comprised of at least a type and an optional
 * payload. Expressing actions as classes enables powerful 
 * type checking in reducer functions.
 * 
 * See Discriminated Unions: https://www.typescriptlang.org/docs/handbook/advanced-types.html#discriminated-unions
 */
export class LoginUserFacebookAction implements Action{
	type = ActionTypes.LOGIN_USER_FACEBOOK;
	constructor () {}
}

export class LoginUserGoogleAction implements Action{
	type = ActionTypes.LOGIN_USER_GOOGLE;
	constructor () {}
}

export class LogoutUserAction implements Action{
	type = ActionTypes.LOGOUT_USER;
	constructor () {}
}

export class SetUserAction implements Action{
	type = ActionTypes.SET_USER;
	constructor (public payload: IUserWithKey) {}
}

export class SetNoUserAction implements Action{
	type = ActionTypes.SET_NO_USER;
	constructor () {}
}

export class AddUserLeagueAction implements Action{
	type = ActionTypes.ADD_USER_LEAGUE;
	constructor (public payload: IUserLeagueRef) {}
}

export class AddUserLeagueCompleteAction implements Action{ // NOOP ACTION. TODO:: Delete league if false
	type = ActionTypes.ADD_USER_LEAGUE_COMPLETE;
	constructor (public payload: boolean) {} 
}

export class AddWrestlerHighlightAction implements Action{
	type = ActionTypes.ADD_WRESTLER_HIGHLIGHT;
	constructor (public payload: { wrestler: IWrestler, highlight: IHighlightDetails}) {}
}

export class AddWrestlerHighlightCompleteAction implements Action{
	type = ActionTypes.ADD_WRESTLER_HIGHLIGHT_COMPLETE;
	constructor (public payload: IWrestlerHighlight) {} 
}

export class RemoveWrestlerHighlightAction implements Action{
	type = ActionTypes.REMOVE_WRESTLER_HIGHLIGHT;
	constructor (public payload: IWrestler) {}
}

export class RemoveWrestlerHighlightCompleteAction implements Action{
	type = ActionTypes.REMOVE_WRESTLER_HIGHLIGHT_COMPLETE;
	constructor (public payload: number) {}
}
