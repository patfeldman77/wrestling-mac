import { IWrestlerHighlight } from './../../info/highlights.class';
import { IUserLeagueRef } from './user-league.interface';
export interface IUser{
	name: string;
	id: string;
	email: string;
	photo: string;
	leagues_ref: dictionary<IUserLeagueRef>;
	highlights: IWrestlerHighlight[]
}
export interface IUserWithKey extends IUser{
	$key: string;
}

export interface IDraftUser extends IUser{
	draftOrder: number;
	key: string;
}

export interface INewDraftUser{
	name: string;
	draftOrder: number;
}