
export interface IUserLeagueRef{
	leagueKey: string;
	isPrivate: boolean;
	leagueName: string;
	draftDate: number;
	creator: string;
	creatorId: string;
}