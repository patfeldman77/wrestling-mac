export enum EDecisionType {
	BYE,
	DEFAULT,
	FORFEIT,
	DISQUALIFICATION,
	FALL,
	TECH_FALL_WITH_NEAR_FALL,
	TECH_FALL,
	DECISION,
	MAJOR,

}


export interface IScore {
	matchDesc: string;
	winner: string;
	loser: string;
	decision: string;
	decisionType: EDecisionType;
	result: string;
	teamPoints: number;
	advancementPoints: number;
	bonusPoints: number;
	placementPoints: number;

}