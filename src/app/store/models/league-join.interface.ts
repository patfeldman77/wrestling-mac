export interface IJoinLeague {
	leagueName: string;
	leaguePassword: string;
	leagueDraftDate: number;
	leagueKey: string;
	leagueCreator: string;
	leagueCreatorId: string;
	isPrivate: boolean;
}

export interface IJoinLeagueStatus {
	status: eJoinStatus;
	leagueName: string;
}

export enum eJoinStatus{
	Nothing,
	Joining,
	Joined,
	Error
}

