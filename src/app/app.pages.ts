import { IPage } from './league-pages/models/page.interface';
import { JoinLeaguePage } from './../pages/join-league/join-league';
import { HomePage } from './../pages/home/home';
import { CreateLeaguePage } from './pages/create-league/create-league';

// set our app's pages
export let AllPages: IPage[] = [
	{ 
		name: 'Home Page', component: HomePage, isRoot: true,
		children: [
			{ 
				name: 'Create League', 
				longName: 'Create New League',
				titleLogo:'assets/img/create.png', 
				component: CreateLeaguePage, 
				logo:  'assets/img/create.png',
				children: [] 
			},
			{ 
				name: 'Join League', 
				longName: 'Join Existing League',
				titleLogo:'assets/img/join.png', 
				component: JoinLeaguePage, 
				logo:  'assets/img/join.png',
			},
		]
	},
];
