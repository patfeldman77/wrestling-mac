import { ILeagueTeam } from './../store/models/league-team.interface';
import { IWrestler } from './../store/models/wrestlers.interface';
import { Injectable, Pipe } from '@angular/core';

@Pipe({
	name: 'orderByScore'
})
@Injectable()
export class OrderByScorePipe {
	transform(teams: ILeagueTeam[]): ILeagueTeam[] {
		if (!teams) return [];
		return teams.sort((a,b) => a.teamScore - b.teamScore);
	}
}
