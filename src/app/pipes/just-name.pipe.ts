import { Injectable, Pipe } from '@angular/core';
@Pipe({
	name: 'justName'
})
@Injectable()
export class JustNamePipe {
	transform(namePlus: string): string {
		if (!namePlus) return '';
		return namePlus.slice(0, namePlus.indexOf('('));
	}
}
