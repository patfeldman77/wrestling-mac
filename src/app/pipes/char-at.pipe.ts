import { Injectable, Pipe } from '@angular/core';

@Pipe({
	name: 'charAt'
})
@Injectable()
export class CharAtPipe {
	transform(fullString: string, charAt: number = 0): string {
		if (!fullString) return '';
		return fullString.slice(charAt, charAt+1);
	}
}
