import { Injectable, Pipe } from '@angular/core';
import * as _ from 'lodash';

@Pipe({
	name: 'values'
})
@Injectable()
export class ValuesPipe {
	transform(objectArray: {}): any[] {
		if (!objectArray) return [];
		return _.map(objectArray, (value, key) => value);
	}
}
