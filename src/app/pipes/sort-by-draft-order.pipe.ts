import { Rules, RuleType } from './../util/rules.class';
import { IDraftUser } from './../store/models/user.interface';
import { Injectable, Pipe } from '@angular/core';

@Pipe({
	name: 'sortByDraftOrder'
})
@Injectable()
export class SortByDraftOrderPipe {
	transform(users: IDraftUser[], numSelected: number, addExtra: boolean = false): IDraftUser[] {
		if (!users) return [];
		if (!numSelected) numSelected = 0;
		// TODO make the rule injected from league data
		let totalSelections = Rules.NumberOrRounds(RuleType.EleventhPick) * users.length;
		if( numSelected >= totalSelections) return [];

		let numReturned = addExtra? users.length + 1 : users.length;
		let maxShow = totalSelections - numSelected;
		numReturned = Math.min(maxShow, numReturned);
		let clone = [...users.sort((a: IDraftUser,b: IDraftUser) => a.draftOrder - b.draftOrder)];
		let dupUsers = [...clone.slice(), ...clone.slice().reverse(), ...clone.slice()];
		let nextUp = numSelected % (users.length*2);
		return dupUsers.slice(nextUp, nextUp+numReturned); 
	}

	public isItMe(users: IDraftUser[], numSelected: number, myId: string): boolean{
		let userList = this.transform(users, numSelected);
		if (userList.length > 0 ){
			return (userList[0].id === myId);
		}
		return false;
	}

	public getCurrentUser(users: IDraftUser[], numSelected: number) : IDraftUser{
		let userList = this.transform(users, numSelected);
		if (userList.length > 0 ){
			return userList[0];
		}
		return null;
	}
}
