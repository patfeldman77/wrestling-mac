import { Injectable, Pipe } from '@angular/core';
import { ELogin } from './../store/models/login.enum';

@Pipe({
  name: 'isLoggedIn'
})
@Injectable()
export class IsLoggedInPipe {
  transform(value: ELogin): boolean {
	  return value === ELogin.LoggedIn;
  }
}
