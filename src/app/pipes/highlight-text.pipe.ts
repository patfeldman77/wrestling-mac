import { IWrestlerHighlight, defaultHighlights } from './../info/highlights.class';
import { Injectable, Pipe } from '@angular/core';

@Pipe({
	name: 'highlightText'
})
@Injectable()
export class HighlightTextPipe {
	transform(highlight: IWrestlerHighlight): string {
		if (!highlight) return '';
		return defaultHighlights.find(type => type.id === highlight.highlightId).textColor;
	}
}
