import { IWrestler } from './../store/models/wrestlers.interface';
import { Injectable, Pipe } from '@angular/core';

@Pipe({
	name: 'byWeight'
})
@Injectable()
export class WrestlersByWeightPipe {
	transform(wrestlers: IWrestler[], weight: string): IWrestler[] {
		if (!wrestlers) return [];
		return wrestlers.filter(wrestler => {
			return !!wrestler && wrestler.weight == weight;
		}).sort((a: IWrestler, b: IWrestler) => a.rank - b.rank);
	}
}
