import { Injectable, Pipe } from '@angular/core';
@Pipe({
	name: 'stripParens'
})
@Injectable()
export class StripParensPipe {
	transform(parenString: string): string {
		if (!parenString) return '';
		return parenString.replace('(', '').replace(')','');
	}
}
