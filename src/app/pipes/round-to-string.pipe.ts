import { Injectable, Pipe } from '@angular/core';
@Pipe({
	name: 'roundToString'
})
@Injectable()
export class RoundToStringPipe {
	transform(desc: string): string {
		if (!desc) return '';
		return desc;
	}
}
