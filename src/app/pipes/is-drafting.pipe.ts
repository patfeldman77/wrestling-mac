import { IDraftSetup } from './../store/models/league.interface';
import { Injectable, Pipe } from '@angular/core';

@Pipe({
	name: 'isDrafting'
})
@Injectable()
export class IsDraftingPipe {
	transform(draft: IDraftSetup): boolean {
		if (!draft|| !draft.draftDate) return false; // FIX THIS PBF false;
		return (draft.draftDate < Date.now());
	}
}
