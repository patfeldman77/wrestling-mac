import { IWrestler } from './../store/models/wrestlers.interface';
import { Injectable, Pipe } from '@angular/core';

@Pipe({
	name: 'totalScore'
})
@Injectable()
export class TotalScorePipe {
	transform(wrestler: IWrestler): number {
		if (!wrestler) return 0;
		let sum = 0;
		wrestler.matches.forEach(match => {
			sum += match.teamPoints
		})
		return sum;
	}
}
