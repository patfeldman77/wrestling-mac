import { ILeague } from './../store/models/league.interface';
import { Injectable, Pipe } from '@angular/core';
import * as _ from 'lodash';

@Pipe({
	name: 'isLeagueMember'
})
@Injectable()
export class IsLeagueMemberPipe {
	transform(league: ILeague, uid: string): boolean {
		if (!league || !league.draftSetup) return false;
		let isMember: boolean = false;
		_.forOwn(league.draftSetup.users, user => {
			if (user.id === uid) {
				isMember = true;
				return;
			}
		});
		return isMember;
	}
}