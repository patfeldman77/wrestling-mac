import { IDraftedWrestler } from './../store/models/league.interface';
import { IWrestler } from './../store/models/wrestlers.interface';
import { Observable } from 'rxjs/Rx';
import { AppState } from './../store/wrestling.store';
import { Store } from '@ngrx/store';
import { Injectable, Pipe } from '@angular/core';
import * as fromRoot from '../store/wrestling.store';

@Pipe({
	name: 'wrestlersIdsToInfo'
})
@Injectable()
export class WrestlersIdsToInfoPipe {
	constructor(private _store: Store<AppState>) { }
	transform(selectedWrestlers: IDraftedWrestler[]): Observable<IWrestler[]> {
		if (!selectedWrestlers) return null;
		let retVal = new Observable<IWrestler[]>(observer => {
			this._store.select(fromRoot.getAllWrestlersById).take(1).subscribe(allWrestlers => {
				let newWrestlers = selectedWrestlers
					.map(wrestler => allWrestlers[wrestler.wrestlerId])
					.sort( (a:IWrestler, b:IWrestler) => parseInt(a.weight) - parseInt(b.weight));
				observer.next(newWrestlers);
				observer.complete();
			})
		});
		return retVal;
	}
}
