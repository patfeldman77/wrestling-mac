import { IDraftedWrestler } from './../store/models/league.interface';
import { Injectable, Pipe } from '@angular/core';

@Pipe({
	name: 'teamByKey'
})
@Injectable()
export class LeagueTeamByKeyPipe {
	transform(team: dictionary<IDraftedWrestler[]>, teamKey: string): IDraftedWrestler[] {
		if (!team) return []
		return team[teamKey];
	}
}
