import { IWrestler } from './../store/models/wrestlers.interface';
import { Injectable, Pipe } from '@angular/core';

@Pipe({
	name: 'totalTeamScore'
})
@Injectable()
export class TotalTeamScorePipe {
	transform(team: IWrestler[]): number {
		if (!team) return 0;
		let sum = 0;
		team.forEach(wrestler =>{
			wrestler.matches.forEach(match => {
				sum += match.teamPoints
			});
		});
		return sum;
	}
}
