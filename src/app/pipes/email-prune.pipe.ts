import { Injectable, Pipe } from '@angular/core';

@Pipe({
	name: 'emailPrune'
})
@Injectable()
export class EmailPrunePipe {
	transform(fullEmailAddress: string): string {
		if (!fullEmailAddress) return '';
		return fullEmailAddress.split('@')[0];
	}
}
