import { IScore } from './../store/models/scores.interface';
import { Injectable, Pipe } from '@angular/core';
@Pipe({
	name: 'reverseMatchOrder'
})
@Injectable()
export class ReverseMatchOrderPipe {
	transform(matches: IScore[]): IScore[] {
		if (!matches) return [];
		// TODO, make this better
		if (matches[0].matchDesc.indexOf("Champ. Round 1") >= 0 || matches[0].matchDesc.indexOf("Prelim") >= 0){
			return matches;
		} else {
			return matches.reverse();
		}

	}
}
