import { IWrestler } from './../store/models/wrestlers.interface';
import { Observable } from 'rxjs/Rx';
import { AppState } from './../store/wrestling.store';
import { Store } from '@ngrx/store';
import { Injectable, Pipe } from '@angular/core';
import * as fromRoot from '../store/wrestling.store';

@Pipe({
	name: 'wrestlerIdToName'
})
@Injectable()
export class WrestlerIdToNamePipe {
	constructor(private _store: Store<AppState>){}
	transform(wrestlerId: string): Observable<IWrestler> {
		if (!wrestlerId) return null;
		let retVal = this._store.select(state => state.wrestlers.wrestlers.wrestlersById[wrestlerId]).take(1);
		return retVal;
	}
}
