import { Injectable, Pipe } from '@angular/core';

@Pipe({
  name: 'keys'
})
@Injectable()
export class KeysPipe {
  transform(objectArray: {}): string[] {
	  if (!objectArray) return []
	  return Object.keys(objectArray);
  }
}
