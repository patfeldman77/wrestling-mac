import { Injectable, Pipe } from '@angular/core';

@Pipe({
	name: 'orderByDraft'
})
@Injectable()
export class OrderByDraftPipe {
	transform(users: any[]): any[] {
		if (!users) return [];
		return users.sort((a: any, b: any) => a.draft_order - b.draft_order);
	}
}
