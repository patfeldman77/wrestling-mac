
157
  Championships
  3rd, 5th, & 7th
  Cons. Semis (32 Man)
  Cons. Round 5
  Cons. Round 4 (32 Man)
  Semis (32 Man)
  Cons. Round 3
  Cons. Round 2 (32 Man)
    Cons. Round 2 - Chase Delande (Edinboro) 23-17 won by major decision over Timothy Ruschell (Wisconsin) 16-11 (MD 9-0)
    Cons. Round 2 - Jake Short (Minnesota) 25-13 won in sudden victory - 1 over Colin Heffernan (Central Michigan) 30-9 (SV-1 7-1)
    Cons. Round 2 - Alex Griffin (Purdue) 26-14 won by decision over Russell Parsons (Army) 27-6 (Dec 8-6)
    Cons. Round 2 - Sal Mastriani (Virginia Tech) 18-7 won by major decision over John Van Brill (Rutgers) 24-11 (MD 10-2)
    Cons. Round 2 - Kyle Langenderfer (Illinois) 25-9 won by major decision over Casey Sparkman (Kent State) 26-14 (MD 9-1)
    Cons. Round 2 - May Bethea (Pennsylvania) 26-10 won by decision over Mike D`Angelo (Princeton) 17-11 (Dec 10-6)
    Cons. Round 2 - Taleb Rahmani (Pittsburgh) 25-7 won by decision over Joshua Shields (Arizona State) 33-9 (Dec 5-3)
    Cons. Round 2 - Archie Colgan (Wyoming) 36-9 won by decision over Victor Lopez (Bucknell) 28-9 (Dec 3-1)
  Quarters
    Quarterfinal - Jason Nolf (Penn State) 25-0 won by fall over Bryant Clagon (Rider) 25-6 (Fall 4:07)
    Quarterfinal - Tyler Berger (Nebraska) 35-4 won in sudden victory - 1 over Joseph Smith (Oklahoma State) 13-5 (SV-1 3-1)
    Quarterfinal - Joey Lavallee (Missouri) 28-1 won by decision over Paul Fox (Stanford) 27-14 (Dec 6-2)
    Quarterfinal - Dylan Palacio (Cornell) 11-1 won by fall over Michael Kemerer (Iowa) 29-3 (Fall 3:28)
  Cons. Round 1
    Cons. Round 1 - Timothy Ruschell (Wisconsin) 16-11 won by decision over Thomas Bullard (NC State) 21-12 (Dec 7-5)
    Cons. Round 1 - Jake Short (Minnesota) 25-13 won by major decision over Jake Faust (Duke) 9-9 (MD 13-4)
    Cons. Round 1 - Alex Griffin (Purdue) 26-14 won by major decision over Clark Glass (Oklahoma) 23-8 (MD 9-1)
    Cons. Round 1 - John Van Brill (Rutgers) 24-11 won by decision over Ryan Mosley (Gardner-Webb) 26-12 (Dec 1-0)
    Cons. Round 1 - Casey Sparkman (Kent State) 26-14 won by decision over Aaron Walker (The Citadel) 28-12 (Dec 5-3)
    Cons. Round 1 - Mike D`Angelo (Princeton) 17-11 won by decision over Colt Shorts (Cal Poly) 21-14 (Dec 9-5)
    Cons. Round 1 - Taleb Rahmani (Pittsburgh) 25-7 won by major decision over Andrew Atkinson (Virginia) 20-12 (MD 18-7)
    Cons. Round 1 - Archie Colgan (Wyoming) 36-9 won by decision over Jake Danishek (Indiana) 19-15 (Dec 9-6)
  Champ Round 2
    Champ. Round 2 - Jason Nolf (Penn State) 25-0 won by tech fall over Victor Lopez (Bucknell) 28-9 (TF-1.5 7:00 (24-9))
    Champ. Round 2 - Bryant Clagon (Rider) 25-6 won by decision over Joshua Shields (Arizona State) 33-9 (Dec 6-5)
    Champ. Round 2 - Joseph Smith (Oklahoma State) 13-5 won by decision over May Bethea (Pennsylvania) 26-10 (Dec 6-3)
    Champ. Round 2 - Tyler Berger (Nebraska) 35-4 won by decision over Kyle Langenderfer (Illinois) 25-9 (Dec 11-7)
    Champ. Round 2 - Joey Lavallee (Missouri) 28-1 won by fall over Sal Mastriani (Virginia Tech) 18-7 (Fall 7:27)
    Champ. Round 2 - Paul Fox (Stanford) 27-14 won by decision over Russell Parsons (Army) 27-6 (Dec 7-5)
    Champ. Round 2 - Dylan Palacio (Cornell) 11-1 won by decision over Colin Heffernan (Central Michigan) 30-9 (Dec 9-5)
    Champ. Round 2 - Michael Kemerer (Iowa) 29-3 won by tech fall over Chase Delande (Edinboro) 23-17 (TF-1.5 6:34 (22-6))
  Consolation Pig Tails
    Prelim - Colt Shorts (Cal Poly) 21-14 won by decision over Clayton Ream (North Dakota State University) 24-6 (Dec 8-5)
  Round 1 (32 Man)
    Champ. Round 1 - Jason Nolf (Penn State) 25-0 won by tech fall over Thomas Bullard (NC State) 21-12 (TF-1.5 7:00 (22-7))
    Champ. Round 1 - Victor Lopez (Bucknell) 28-9 won by decision over Timothy Ruschell (Wisconsin) 16-11 (Dec 5-3)
    Champ. Round 1 - Joshua Shields (Arizona State) 33-9 won by major decision over Jake Faust (Duke) 9-9 (MD 12-1)
    Champ. Round 1 - Bryant Clagon (Rider) 25-6 won by decision over Jake Short (Minnesota) 25-13 (Dec 4-2)
    Champ. Round 1 - Joseph Smith (Oklahoma State) 13-5 won by decision over Alex Griffin (Purdue) 26-14 (Dec 4-3)
    Champ. Round 1 - May Bethea (Pennsylvania) 26-10 won by decision over Clark Glass (Oklahoma) 23-8 (Dec 3-2)
    Champ. Round 1 - Kyle Langenderfer (Illinois) 25-9 won by decision over John Van Brill (Rutgers) 24-11 (Dec 11-4)
    Champ. Round 1 - Tyler Berger (Nebraska) 35-4 won by decision over Ryan Mosley (Gardner-Webb) 26-12 (Dec 9-2)
    Champ. Round 1 - Joey Lavallee (Missouri) 28-1 won by fall over Aaron Walker (The Citadel) 28-12 (Fall 1:27)
    Champ. Round 1 - Sal Mastriani (Virginia Tech) 18-7 won by tech fall over Casey Sparkman (Kent State) 26-14 (TF-1.5 7:00 (20-5))
    Champ. Round 1 - Russell Parsons (Army) 27-6 won by decision over Mike D`Angelo (Princeton) 17-11 (Dec 8-1)
    Champ. Round 1 - Paul Fox (Stanford) 27-14 won by decision over Clayton Ream (North Dakota State University) 24-6 (Dec 8-4)
    Champ. Round 1 - Dylan Palacio (Cornell) 11-1 won by major decision over Taleb Rahmani (Pittsburgh) 25-7 (MD 9-1)
    Champ. Round 1 - Colin Heffernan (Central Michigan) 30-9 won in sudden victory - 1 over Andrew Atkinson (Virginia) 20-12 (SV-1 6-4)
    Champ. Round 1 - Chase Delande (Edinboro) 23-17 won by decision over Archie Colgan (Wyoming) 36-9 (Dec 8-2)
    Champ. Round 1 - Michael Kemerer (Iowa) 29-3 won by major decision over Jake Danishek (Indiana) 19-15 (MD 14-3)
  Pig Tails
    Prelim - Bryant Clagon (Rider) 25-6 won by major decision over Colt Shorts (Cal Poly) 21-14 (MD 11-2)