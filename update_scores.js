var _ = require('lodash');
var fs = require('fs');

// var sc = require('./../../app/util/score.convert.js');
// var iw = require('./../../app/store/models/wrestlers.interface.js')
var weights = ['125', '133', '141', '149', '157', '165', '174', '184', '197', '285'];

var EDecisionType;
(function (EDecisionType) {
    EDecisionType[EDecisionType["BYE"] = 0] = "BYE";
    EDecisionType[EDecisionType["DEFAULT"] = 1] = "DEFAULT";
    EDecisionType[EDecisionType["FORFEIT"] = 2] = "FORFEIT";
    EDecisionType[EDecisionType["DISQUALIFICATION"] = 3] = "DISQUALIFICATION";
    EDecisionType[EDecisionType["FALL"] = 4] = "FALL";
    EDecisionType[EDecisionType["TECH_FALL_WITH_NEAR_FALL"] = 5] = "TECH_FALL_WITH_NEAR_FALL";
    EDecisionType[EDecisionType["TECH_FALL"] = 6] = "TECH_FALL";
    EDecisionType[EDecisionType["DECISION"] = 7] = "DECISION";
    EDecisionType[EDecisionType["MAJOR"] = 8] = "MAJOR";
})(EDecisionType || (EDecisionType = {}));

var EPigTailType;
(function (EPigTailType) {
    EPigTailType[EPigTailType["CHAMP"] = 0] = "CHAMP";
    EPigTailType[EPigTailType["CONSOS"] = 1] = "CONSOS";
})(EPigTailType || (EPigTailType = {}));

var ScoreConvert = (function () {
    function ScoreConvert() {
    }
    ScoreConvert.Convert = function (wrestlersAtWeight, weight, input) {
        var _this = this;
        var lines = input.split('\n');

        wrestlersAtWeight.forEach(function (wrestler) {
            var thisWrestlerStats = [];
            var lastCat = 1 /* CONSOS */;
            lines.forEach(function (line) {
                if (line.indexOf('Conso Pig Tails')) {
                    lastCat = 1 /* CONSOS */;
                } else if (line.indexOf('Pig Tails')) {
                    lastCat = 0 /* CHAMP */;
                }
                if (line.indexOf(wrestler.last_name) >= 0) {
                    var details = _this.ParseLine(line, wrestler, lastCat);
                    if (!!details) {
                        if (details.loser.toLowerCase().indexOf(wrestler.last_name.toLowerCase()) >= 0) {
                            if (_this.CheckFirstName(details.loser, wrestler)) {
                                thisWrestlerStats.push(details);
                            }
                        } else if (details.winner.toLowerCase().indexOf(wrestler.last_name.toLowerCase()) >= 0) {
                            if (_this.CheckFirstName(details.winner, wrestler)) {
                                thisWrestlerStats.push(details);
                            }
                        } else {
                            console.log("NAME WARNING:::NOT IN WINNER OR LOSER " + details.loser + " OR " + details.winner + "/" + wrestler.first_name + " " + wrestler.last_name + ":: MIGHT BE A FIRST NAME/LAST NAME CONFUSION");
                        }
                    }
                }
            });
            wrestler.matches = thisWrestlerStats;
        });
    };

    ScoreConvert.CheckFirstName = function (name, wrestler) {
        if (name.toLowerCase().indexOf(wrestler.first_name.toLowerCase()) >= 0) {
            return true;
        }
        console.log("NAME WARNING:: First Names didn't match::" + name + "/" + wrestler.first_name + " " + wrestler.last_name);
        return false;
    };
    ScoreConvert.ParseLine = function (line, wrestler, pigTailCat) {
        var matchDesc = line.slice(0, line.indexOf(" - ")).trim();
        var rest = line.slice(line.indexOf(" - ") + 3);
        var decision = rest.slice(rest.indexOf("won") + 3, rest.indexOf("over")).trim();
        var winner = rest.slice(0, rest.indexOf("won")).trim();
        rest = rest.slice(rest.indexOf("over") + 4);
        var endPoint = rest.split("(", 2).join("(").length;
        var loser = rest.slice(0, endPoint).trim();
        var result = rest.slice(endPoint).trim();
        var advancementPoints = 0;
        var bonusPoints = 0;
        var placementPoints = 0;
        var decisionType = 7 /* DECISION */;
        if (winner.indexOf(wrestler.last_name) >= 0) {
            if (winner.indexOf(wrestler.first_name) >= 0 || winner.indexOf(wrestler.school) >= 0) {
                var decisionStuff = this.GetDecisionType(decision, result);
                bonusPoints = decisionStuff.bonusPoints;
                decisionType = decisionStuff.decisionType;
                var extraPoints = this.GetExtraPoints(matchDesc, pigTailCat);
                advancementPoints = extraPoints.advancementPoints;
                placementPoints = extraPoints.placementPoints;
            } else {
                return null;
            }
        }
        var totalPoints = advancementPoints + bonusPoints + placementPoints;
        var retVal = {
            matchDesc: matchDesc,
            winner: winner,
            decision: decision,
            loser: loser,
            decisionType: decisionType,
            result: result,
            teamPoints: totalPoints,
            advancementPoints: advancementPoints,
            bonusPoints: bonusPoints,
            placementPoints: placementPoints
        };

        return retVal;
    };
    ScoreConvert.GetDecisionType = function (decision, result) {
        var retVal = { bonusPoints: 0, decisionType: 7 /* DECISION */ };
        if (decision.indexOf("major decision") >= 0) {
            retVal.bonusPoints = 1;
            retVal.decisionType = 8 /* MAJOR */;
        } else if (decision.indexOf("tech fall") >= 0) {
            if (result.indexOf("TF-1.5") >= 0) {
                retVal.bonusPoints = 1.5;
                retVal.decisionType = 5 /* TECH_FALL_WITH_NEAR_FALL */;
            } else {
                retVal.bonusPoints = 1;
                retVal.decisionType = 6 /* TECH_FALL */;
            }
        } else if (decision.indexOf("fall") >= 0) {
            retVal.bonusPoints = 2;
            retVal.decisionType = 4 /* FALL */;
        } else if (decision.indexOf("forfeit") >= 0) {
            retVal.bonusPoints = 2;
            retVal.decisionType = 2 /* FORFEIT */;
        } else {
            retVal.decisionType = 7 /* DECISION */;
        }
        return retVal;
    };

    ScoreConvert.GetExtraPoints = function (matchDesc, pigTailCat) {
        var retVal = { advancementPoints: 0, placementPoints: 0 };
        if (matchDesc.indexOf("Prelim") >= 0) {
            if (pigTailCat == 0 /* CHAMP */) {
                retVal.advancementPoints = 1;
            } else {
                retVal.advancementPoints = .5;
            }
        } else if (matchDesc.indexOf("Champ. Round 1") >= 0) {
            retVal.advancementPoints = 1;
        } else if (matchDesc.indexOf("Champ. Round 2") >= 0) {
            retVal.advancementPoints = 1;
        } else if (matchDesc.indexOf("Quarterfinal") >= 0) {
            retVal.advancementPoints = 1;
            retVal.placementPoints = 6;
        } else if (matchDesc.indexOf("Semifinal") >= 0) {
            retVal.advancementPoints = 1;
            retVal.placementPoints = 6;
        } else if (matchDesc.indexOf("Cons. Round 1") >= 0) {
            retVal.advancementPoints = .5;
        } else if (matchDesc.indexOf("Cons. Round 2") >= 0) {
            retVal.advancementPoints = .5;
        } else if (matchDesc.indexOf("Cons. Round 3") >= 0) {
            retVal.advancementPoints = .5;
        } else if (matchDesc.indexOf("Cons. Round 4") >= 0) {
            retVal.advancementPoints = .5;
            retVal.placementPoints = 3;
        } else if (matchDesc.indexOf("Cons. Round 5") >= 0) {
            retVal.advancementPoints = .5;
            retVal.placementPoints = 3;
        } else if (matchDesc.indexOf("Cons. Semi") >= 0) {
            retVal.advancementPoints = .5;
            retVal.placementPoints = 3;
        } else if (matchDesc.indexOf("1st Place") >= 0) {
            retVal.placementPoints = 4;
        } else if (matchDesc.indexOf("3rd Place") >= 0) {
            retVal.placementPoints = 1;
        } else if (matchDesc.indexOf("5th Place") >= 0) {
            retVal.placementPoints = 1;
        } else if (matchDesc.indexOf("7th Place") >= 0) {
            retVal.placementPoints = 1;
        }

        return retVal;
    };
    return ScoreConvert;
})();

var allWrestlers = JSON.parse(fs.readFileSync('src/assets/data/participants.2017.json', 'utf8'));

var allWrestlersByWeight = {};

_.forEach(allWrestlers, function (data) {
    _.forEach(data, function (wrestler, index) {
        wrestler.id = index;
    });

    _.forEach(weights, function (weight) {
        allWrestlersByWeight[weight] = _.filter(data, function (wrestler) {
            return wrestler.weight == weight;
        });
    });
});

weights.forEach(function (weight) {
    var wrestlers = allWrestlersByWeight[weight];
    var file = fs.readFileSync('src/assets/data/results.' + weight.toString() + '.txt', 'utf8');
    ScoreConvert.Convert(wrestlers, weight, file);
});

var output = JSON.stringify(allWrestlers);
fs.writeFileSync('src/assets/data/participants.2017.scores.json', output);
